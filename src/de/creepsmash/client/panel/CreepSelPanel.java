/**
 Creep Smash, a multiplayer towerdefence game
 created as a project at the Hochschule fuer
 Technik Stuttgart (University of Applied Science)
 http://www.hft-stuttgart.de 

 Copyright (C) 2008 by      
 * Andreas Wittig
 * Bernd Hietler
 * Christoph Fritz
 * Fabian Kessel
 * Levin Fritz
 * Nikolaj Langner
 * Philipp Schulte-Hubbert
 * Robert Rapczynski
 * Ron Trautsch
 * Sven Supper
 http://creepsmash.sf.net/

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

package de.creepsmash.client.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import de.creepsmash.client.game.ContextListener;
import de.creepsmash.client.game.GameContext;
import de.creepsmash.common.IConstants;

/**
 * Panel with Buttons to select Creeps.
 * 
 * @author sven
 */

public class CreepSelPanel extends JPanel implements ContextListener {

	private static Logger logger = Logger.getLogger(CreepSelPanel.class
			.getName());
	private static final long serialVersionUID = -5978301134543431476L;

	private Font font;
	private Font labelFont;

	private final int height;
	private final int width;

	private JPanel buttonPanel;
	private JButton button1;
	private JButton button2;
	private JButton button3;
	private JButton button4;
	private JButton button5;
	private JButton button6;
	private JButton button7;
	private JButton button8;
	private JButton button9;
	private JButton button10;
	private JButton button11;
	private JButton button12;
	private JButton button13;
	private JButton button14;
	private JButton button15;
	private JButton button16;

	//private JCheckBox autoSendBox;

	private GameContext context;
	private GamePanel gamepanel;
	private JLabel[] labels = new JLabel[17];
	

	/**
	 * @return the context
	 */
	public GameContext getContext() {
		return context;
	}

	/**
	 * sets Layout to Gridlayout. adds Buttons to the Panel
	 * 
	 * @param width
	 *            of the Panel
	 * @param height
	 *            of the Panel
	 * @param gamepanel
	 *            for getting object Gamepanel
	 */
	public CreepSelPanel(GamePanel gamepanel, int width, int height) {
		this.height = height;
		this.width = width;
		this.gamepanel = gamepanel;
		this.setPreferredSize(new Dimension(this.width, this.height));
		this.setSize(this.width, this.height);
		this.setLayout(new BorderLayout());
		font = new Font("Helvetica", Font.PLAIN, 6);
		labelFont = new Font("Helvetica", Font.PLAIN, 10);
		init();

		add(buttonPanel, java.awt.BorderLayout.CENTER);
		
	}

	/**
	 * init method to which inits the object of the Panel.
	 */
	private void init() {
		this.setBackground(Color.BLACK);

		buttonPanel = new JPanel();
		buttonPanel.setLayout(new GridLayout(4, 4));

		button1 = new CreepSendButton(gamepanel, IConstants.Creeps.creep1, "creepIcon1");
		button2 = new CreepSendButton(gamepanel, IConstants.Creeps.creep2, "creepIcon2");
		button3 = new CreepSendButton(gamepanel, IConstants.Creeps.creep3, "creepIcon3");
		button4 = new CreepSendButton(gamepanel, IConstants.Creeps.creep4, "creepIcon4");
		button5 = new CreepSendButton(gamepanel, IConstants.Creeps.creep5, "creepIcon5");
		button6 = new CreepSendButton(gamepanel, IConstants.Creeps.creep6, "creepIcon6");
		button7 = new CreepSendButton(gamepanel, IConstants.Creeps.creep7, "creepIcon7");
		button8 = new CreepSendButton(gamepanel, IConstants.Creeps.creep8, "creepIcon8");
		button9 = new CreepSendButton(gamepanel, IConstants.Creeps.creep9, "creepIcon9");
		button10 = new CreepSendButton(gamepanel, IConstants.Creeps.creep10, "creepIcon10");
		button11 = new CreepSendButton(gamepanel, IConstants.Creeps.creep11, "creepIcon11");
		button12 = new CreepSendButton(gamepanel, IConstants.Creeps.creep12, "creepIcon12");
		button13 = new CreepSendButton(gamepanel, IConstants.Creeps.creep13, "creepIcon13");
		button14 = new CreepSendButton(gamepanel, IConstants.Creeps.creep14, "creepIcon14");
		button15 = new CreepSendButton(gamepanel, IConstants.Creeps.creep15, "creepIcon15");
		button16 = new CreepSendButton(gamepanel, IConstants.Creeps.creep16, "creepIcon16");
	
		buttonPanel.add(button1);
		buttonPanel.add(button2);
		buttonPanel.add(button3);
		buttonPanel.add(button4);
		buttonPanel.add(button5);
		buttonPanel.add(button6);
		buttonPanel.add(button7);
		buttonPanel.add(button8);
		buttonPanel.add(button9);
		buttonPanel.add(button10);
		buttonPanel.add(button11);
		buttonPanel.add(button12);
		buttonPanel.add(button13);
		buttonPanel.add(button14);
		buttonPanel.add(button15);
		buttonPanel.add(button16);

	}

	/**
	 * Setter to set Context.
	 * 
	 * @param context
	 *            to specify which context
	 */
	public void setContext(GameContext context) {
		this.context = context;
		context.addContextListener(this);
	}

	/**
	 * Enable/disable Buttons according to income.
	 */
	public synchronized void updateButtons() {
		if (context == null) {
			return;
		}
		int credits = context.getCredits();

		if ((credits >= IConstants.Creeps.creep1.getPrice())) {
			this.button1.setEnabled(true);
		} else {
			this.button1.setEnabled(false);
		}
		if ((credits >= IConstants.Creeps.creep2.getPrice())) {
			this.button2.setEnabled(true);
		} else {
			this.button2.setEnabled(false);
		}
		if ((credits >= IConstants.Creeps.creep3.getPrice())) {
			this.button3.setEnabled(true);
		} else {
			this.button3.setEnabled(false);
		}
		if ((credits >= IConstants.Creeps.creep4.getPrice())) {
			this.button4.setEnabled(true);
		} else {
			this.button4.setEnabled(false);
		}
		if ((credits >= IConstants.Creeps.creep5.getPrice())) {
			this.button5.setEnabled(true);
		} else {
			this.button5.setEnabled(false);
		}
		if ((credits >= IConstants.Creeps.creep6.getPrice())) {
			this.button6.setEnabled(true);
		} else {
			this.button6.setEnabled(false);
		}
		if ((credits >= IConstants.Creeps.creep6.getPrice())) {
			this.button6.setEnabled(true);
		} else {
			this.button6.setEnabled(false);
		}

		if ((credits >= IConstants.Creeps.creep6.getPrice())) {
			this.button6.setEnabled(true);
		} else {
			this.button6.setEnabled(false);
		}
		if ((credits >= IConstants.Creeps.creep7.getPrice())) {
			this.button7.setEnabled(true);
		} else {
			this.button7.setEnabled(false);
		}
		if ((credits >= IConstants.Creeps.creep8.getPrice())) {
			this.button8.setEnabled(true);
		} else {
			this.button8.setEnabled(false);
		}
		if ((credits >= IConstants.Creeps.creep9.getPrice())) {
			this.button9.setEnabled(true);
		} else {
			this.button9.setEnabled(false);
		}
		if ((credits >= IConstants.Creeps.creep9.getPrice())) {
			this.button9.setEnabled(true);
		} else {
			this.button9.setEnabled(false);
		}
		if ((credits >= IConstants.Creeps.creep10.getPrice())) {
			this.button10.setEnabled(true);
		} else {
			this.button10.setEnabled(false);
		}
		if ((credits >= IConstants.Creeps.creep11.getPrice())) {
			this.button11.setEnabled(true);
		} else {
			this.button11.setEnabled(false);
		}
		if ((credits >= IConstants.Creeps.creep12.getPrice())) {
			this.button12.setEnabled(true);
		} else {
			this.button12.setEnabled(false);
		}
		if ((credits >= IConstants.Creeps.creep13.getPrice())) {
			this.button13.setEnabled(true);
		} else {
			this.button13.setEnabled(false);
		}
		if ((credits >= IConstants.Creeps.creep14.getPrice())) {
			this.button14.setEnabled(true);
		} else {
			this.button14.setEnabled(false);
		}
		if ((credits >= IConstants.Creeps.creep15.getPrice())) {
			this.button15.setEnabled(true);
		} else {
			this.button15.setEnabled(false);
		}
		if ((credits >= IConstants.Creeps.creep16.getPrice())) {
			this.button16.setEnabled(true);
		} else {
			this.button16.setEnabled(false);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void actionPerformed(ActionEvent arg0) {
	}

	/**
	 * {@inheritDoc}
	 */
	public void creditsChanged(GameContext context) {
		gamepanel.getCreepPanel().updateButtons();

	}

	/**
	 * {@inheritDoc}
	 */
	public void incomeChanged(GameContext context) {
		// TODO Auto-generated method stub

	}

	/**
	 * {@inheritDoc}
	 */
	public void livesChanged(GameContext context) {
		// TODO Auto-generated method stub

	}

	/**
	 * {@inheritDoc}
	 */
	public void selectedChanged(GameContext context, String message) {
		// TODO Auto-generated method stub

	}

	/**
	 * @return the gamepanel
	 */
	public GamePanel getGamepanel() {
		return gamepanel;
	}

	/**
	 * @param gamepanel
	 *            the gamepanel to set
	 */
	public void setGamepanel(GamePanel gamepanel) {
		this.gamepanel = gamepanel;
	}
	
	public void disableButtons() {
		if (context == null) {
			return;
		}

			this.button1.setEnabled(false);
			this.button2.setEnabled(false);
			this.button3.setEnabled(false);
			this.button4.setEnabled(false);
			this.button5.setEnabled(false);
			this.button6.setEnabled(false);
			this.button7.setEnabled(false);
			this.button8.setEnabled(false);
			this.button9.setEnabled(false);
			this.button10.setEnabled(false);
			this.button11.setEnabled(false);
			this.button12.setEnabled(false);
			this.button13.setEnabled(false);
			this.button14.setEnabled(false);
			this.button15.setEnabled(false);
			this.button16.setEnabled(false);
	}

}
