package de.creepsmash.client.panel;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.NumberFormat;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;

import de.creepsmash.client.game.GameContext;
import de.creepsmash.common.IConstants;

public class TowerBuyButton extends JButton implements ActionListener,
		MouseListener {

	private static final long serialVersionUID = 1L;

	private GamePanel gamepanel;
	private IConstants.Towers type;

	public TowerBuyButton(GamePanel gamepanel, IConstants.Towers type,
			String iconName) {
		super();
		this.gamepanel = gamepanel;
		this.type = type;
		Font font = new Font("Helvetica", Font.PLAIN, 9);
		setFont(font);
		setBackground(Color.BLACK);
		setBorderPainted(false);
		setForeground(Color.GREEN);
		setIcon(createIcon(iconName));
		setDisabledIcon(createIcon(iconName + "disable"));
		setEnabled(false);
		addActionListener(this);
		addMouseListener(this);
	}

	private Icon createIcon(String name) {
		// FIXME This should be a Constant (in IConstants?)
		String path = "de/creepsmash/client/resources/pictures/";
		return new ImageIcon(getClass().getClassLoader().getResource(
				path + name + ".png"));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		GameContext context = gamepanel.getContext();
		if (context.isDead()) {
			context.setSelectedTower(null);
			return;
		}
		context.setNextTower(type);
		context.getGameBoard().deSelectTowers();
		context.setSelectedTower(null);
		gamepanel.getSelectTowerInfoPanel().setVisible(false);
		gamepanel.setLastTowerInfoPanel(gamepanel.getBuildTowerInfoPanel());
		updateTowerInfo();
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		gamepanel.getLastTowerInfoPanel().setVisible(false);
		updateTowerInfo();
	}

	@Override
	public void mouseExited(MouseEvent e) {
		gamepanel.getTowerInfoPanel().setVisible(false);
		gamepanel.getLastTowerInfoPanel().setVisible(true);
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	private void updateTowerInfo() {
		BuildTowerInfoPanel towerInfo = gamepanel.getTowerInfoPanel();
		towerInfo.getNameInfo().setText(type.getName());
		towerInfo.getPriceInfo().setText("Price: " + format(type.getPrice()));
		towerInfo.getDamageInfo()
				.setText("Damage: " + format(type.getDamage()));
		towerInfo.getSpeedInfo().setText("Speed: " + type.getSpeedString());
		towerInfo.getRangeInfo().setText("Range: " + (int) type.getRange());
		towerInfo.getSpecialInfo().setText(type.getSpecial());
		towerInfo.setVisible(true);
	}

	private String format(int value) {
		return NumberFormat.getInstance().format(value);
	}
}
