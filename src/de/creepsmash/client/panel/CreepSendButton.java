package de.creepsmash.client.panel;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.NumberFormat;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;

import de.creepsmash.client.game.GameContext;
import de.creepsmash.common.IConstants;

public class CreepSendButton extends JButton implements ActionListener, MouseListener {

	private static final long serialVersionUID = 1L;
	
	private GamePanel gamepanel;
	private IConstants.Creeps type;

	public CreepSendButton(GamePanel gamepanel, IConstants.Creeps type, String iconName) {
		super();
		this.gamepanel = gamepanel;
		this.type = type;
		Font font = new Font("Helvetica", Font.PLAIN, 6);
		setFont(font);
		setBackground(Color.BLACK);
		setBorderPainted(false);
		setForeground(Color.RED);
		setIcon(createIcon(iconName));
		setDisabledIcon(createIcon(iconName + "disable"));
		setEnabled(false);
		addActionListener(this);
		addMouseListener(this);
	}

	private Icon createIcon(String name) {
		// FIXME This should be a Constant (in IConstants?)
		String path = "de/creepsmash/client/resources/pictures/";
		return new ImageIcon(getClass().getClassLoader().getResource(path + name + ".png"));
	}

	@Override
	public synchronized void actionPerformed(ActionEvent e) {
		if (gamepanel.getCreepPanel().getContext() != null) {
			GameContext context = gamepanel.getCreepPanel().getContext();
			if (!context.isDead()) {
				if (((e.getModifiers() & ActionEvent.SHIFT_MASK) > 0) && context.readyForNewWave()) {
					context.sendCreepsWave(context, gamepanel, type);
				} else if (context.readyForNewCreep() && context.readyForNewWave()) {
					context.sendCreep(context, gamepanel, type);
				}
			}
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		gamepanel.getLastTowerInfoPanel().setVisible(false);
		CreepInfoPanel infoPanel = gamepanel.getCreepInfoPanel();
		infoPanel.getNameInfo().setText(type.getName());
		infoPanel.getPriceInfo().setText("Price: " + format(type.getPrice()));
		infoPanel.getIncomeInfo().setText("income: +" + format(type.getIncome()));
		infoPanel.getHealthInfo().setText("Health: " + format(type.getHealth()));
		infoPanel.getSpeedInfo().setText("Speed: " + type.getSpeedString());
		infoPanel.getSpecialInfo().setText(type.getSpecial());
		infoPanel.setVisible(true);
	}

	@Override
	public void mouseExited(MouseEvent e) {
		gamepanel.getCreepInfoPanel().setVisible(false);
		gamepanel.getLastTowerInfoPanel().setVisible(true);
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	private String format(int value) {
		return NumberFormat.getInstance().format(value);
	}
}
