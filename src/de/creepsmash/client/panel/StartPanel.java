
/**
   Creep Smash, a multiplayer towerdefence game
   created as a project at the Hochschule fuer
   Technik Stuttgart (University of Applied Science)
   http://www.hft-stuttgart.de 
   
   Copyright (C) 2008 by      
    * Andreas Wittig
    * Bernd Hietler
    * Christoph Fritz
    * Fabian Kessel
    * Levin Fritz
    * Nikolaj Langner
    * Philipp Schulte-Hubbert
    * Robert Rapczynski
    * Ron Trautsch
    * Sven Supper
    http://creepsmash.sf.net/

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/


package de.creepsmash.client.panel;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;

import de.creepsmash.client.Core;

/**
 * The welcome screen for the game.
 * @author Philipp
 *
 */
public class StartPanel extends GameScreen {

	private static final long serialVersionUID = 1L;

	private JLabel start;

	/**
	 * Creates a new instance.
	 * @param core the core of the game.
	 */
	public StartPanel(Core core) {
		super();
		
//		this.setLayout(null);
		
		start = new JLabel("<html><center>"
				+ "<FONT color=#00ff33 face=Verdana size=28>" 
				+ "<br><br><br>Creep Smash<br>"
				+ "<FONT color=#D3CA24 size=3><br><br><br>"
				+ "* Multiplayer Vectorized Tower Defence *<br>"
				+ "Click to begin, F1 for Help<br><br><br><br><br>"
				+ "<FONT color=#42BD09>"
				+ "In a human unreachable, plane ( 2D-Graphic ) future<br>"
				+ "<br>"
				+ " - there is somewhere far, far away... -<br>"
				+ "<br>"
			    + "... at the end of an unknown galaxy, an intergalactic "
			    		+ "competition-planet named TW3000D.<br>"
			    + "Only one strong and brave hero of every race is allowed "
			    		+ "to fight there for honor and glory.<br>"
				+ "<br>"
			    + "For a new time!<br>"
				+ "<br>"
			    + "So every comrade-in-arms gets an armory of towers and "
			    		+ "creeps. Creeps are producing solar power energy, "
			    		+ "and caused by that,<br>"
			    + "they increase income! Even after their brain-dead, the "
			    		+ "solar panel will still supply energy, therefore"
			    		+ " income to war chest.<br>"
				+ "Lifepoints indicate health of every combatant. To stand "
						+ "against an opposing assault, the brave hero has to"
						+ " defend<br>"
				+ "his battelzone. This is done by buying and placing towers"
						+ " as smart as possible."
				+ "<br>"
				+ "For a creep, it's feasible to cross the landmark of a "
						+ "hero's battlezone. If this happens, lifepoints will "
						+ "decrease<br>"
				+ "until there are no more left - and if: dead will arise over "
						+ "this looser-battle-land.<br>"
				+ "<br>"
				+ "But the winner, that glory, women's beloved hero with "
						+ "still having lifepoints, will guide his race into "
						+ "a shiny new future,<br>"
				+ "over and over fulfilled with everything you wish him to "
						+ "be with.<br>"
				+ "<br>"
				+ "Will you be that hero?"
			    + "</center></html>");
		start.setBounds(350, 250, 400, 30);

		
	
		this.add(start);
		

		
		this.setBackground(Color.BLACK);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		g.setColor(Color.GREEN);
		Graphics2D g2d = (Graphics2D) g;
		AlphaComposite myAlpha = AlphaComposite.getInstance(
				AlphaComposite.SRC_OVER, 0.22f);
		g2d.setComposite(myAlpha);
		g2d.fillRoundRect(30, 100, Core.WIDTH - 60, 100, 20, 20);
		AlphaComposite noAlpha = AlphaComposite.getInstance(
				AlphaComposite.SRC_OVER, 1.0f);
		g2d.setComposite(noAlpha);
		
		
	}

    
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void end() {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void start() {
		// TODO Auto-generated method stub
	}
	
    /**
	 * {@inheritDoc}
	 */
    public void mouseReleased(MouseEvent e) {
    	
//    	getCore().pushScreen(new GameLobby());
//    	getCore().pushScreen(new GamePanel());
    	getCore().pushScreen(new LoginPanel());
		
    }
}
