/**
   Creep Smash, a multiplayer towerdefence game
   created as a project at the Hochschule fuer
   Technik Stuttgart (University of Applied Science)
   http://www.hft-stuttgart.de 
   
   Copyright (C) 2008 by      
    * Andreas Wittig
    * Bernd Hietler
    * Christoph Fritz
    * Fabian Kessel
    * Levin Fritz
    * Nikolaj Langner
    * Philipp Schulte-Hubbert
    * Robert Rapczynski
    * Ron Trautsch
    * Sven Supper
    http://creepsmash.sf.net/

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

package de.creepsmash.client.panel;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URL;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

/**
 * The HelpGamePanel gives a help for players. It describes how the game works.
 * It gives also informations about our team.
 * 
 * @Robert
 */

public class HelpGamePanel extends JDialog {

	/**
	 * @param args
	 */
	private static final long serialVersionUID = 4L;

	private JEditorPane htmlDisplay = new JEditorPane();
	private JScrollPane jScrollPaneHelp = new JScrollPane();
	private JPanel content = new JPanel();

	private JButton quit;
	
	/**
	 * Constructor for the HelpGamePanel.
	 * @param owner
	 * @param title 
	 */
	public HelpGamePanel(JFrame owner, String title) {
		super(owner, title);
		this.init();
	}

	/**
	 * This method initializes htmlDisplay.
	 * 
	 * @return htmlDisplay an EditorPane
	 */
	private JEditorPane gethtmlDisplay() {

		htmlDisplay.setEditable(false);
		htmlDisplay.setCaretPosition(0);
		htmlDisplay.setContentType("text/html");
		htmlDisplay.setOpaque(false);

		return htmlDisplay;
	}

	/**
	 * This method initializes jScrollPane.
	 * 
	 * @return jScrollPaneHelp an JScrollPane
	 */
	private JScrollPane getJScrollPaneHelp() {

		jScrollPaneHelp.setBounds(10, 10, 600, 600);
		jScrollPaneHelp
				.setHorizontalScrollBarPolicy(jScrollPaneHelp.HORIZONTAL_SCROLLBAR_NEVER);
		jScrollPaneHelp.setViewportView(gethtmlDisplay());
		jScrollPaneHelp.getViewport().setOpaque(false);
		jScrollPaneHelp.setOpaque(false);

		return jScrollPaneHelp;
	}

	/**
	 * Creates a new instance of HelpGamePanel.
	 * 
	 */
	public HelpGamePanel() {
		this.init();
	}

	/**
	 * Initialize the Panel.
	 * 
	 */
	private void init() {
		this.setLayout(null);
		this.setBackground(Color.BLACK);
		this.setResizable(false);
		
		content.setLayout(null);
		content.setBounds(0, 0, 630, 680);
		content.setBackground(Color.BLACK);
		
		this.quit = new JButton("Exit Help");
		this.quit.setBackground(Color.BLACK);
		this.quit.setForeground(Color.GREEN);
		this.quit.setBounds(265, 620, 100, 20);
		this.quit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		
		try {
			final URL url = getClass().getClassLoader().getResource(
					"de/creepsmash/client/resources/help/index.html");
			htmlDisplay.setPage(url);

		} catch (IOException ex) {
			ex.printStackTrace();
			System.out.println("HTML-Seite konnte nicht geladen werden!");
		}

		htmlDisplay.addHyperlinkListener(new HyperlinkListener() {

			public void hyperlinkUpdate(HyperlinkEvent e) {
				try {
					if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
						htmlDisplay.setPage(e.getURL());
					}
				} catch (IOException ex) {
					ex.printStackTrace();
					System.out
							.println("HTML-Seite konnte nicht geladen werden!");
				}
			}

		});

		content.add(getJScrollPaneHelp());
		content.add(quit);
		this.add(content);
	}
	// /**
	// * displays the screen.
	// */
	// @Override
	// public void start() {
	//  
	//    
	// }
	//
	// /**
	// * method for disappearing the screen.
	// */
	// @Override
	// public void end() {
	//  
	// }

}

