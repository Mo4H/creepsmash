/**
   Creep Smash, a multiplayer towerdefence game
   created as a project at the Hochschule fuer
   Technik Stuttgart (University of Applied Science)
   http://www.hft-stuttgart.de 
   
   Copyright (C) 2008 by      
    * Andreas Wittig
    * Bernd Hietler
    * Christoph Fritz
    * Fabian Kessel
    * Levin Fritz
    * Nikolaj Langner
    * Philipp Schulte-Hubbert
    * Robert Rapczynski
    * Ron Trautsch
    * Sven Supper
    http://creepsmash.sf.net/

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

/**
 * 
 */
package de.creepsmash.client.panel;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.UIManager;

import de.creepsmash.client.Core;
import de.creepsmash.client.network.MessageListener;
import de.creepsmash.common.IConstants;
import de.creepsmash.common.messages.client.LoginRequestMessage;
import de.creepsmash.common.messages.server.LoginResponseMessage;
import de.creepsmash.common.messages.server.ServerMessage;

/**
 * LoginPanel at the beginning of the game.
 * @author sven
 *
 */
public class LoginPanel extends GameScreen implements MessageListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JLabel name;
	private JLabel password;
	private JLabel jlogin;
	
	private JTextField lName;
	private JPasswordField lPassword;
	
	private JButton login;
	private JButton register;
	private JButton exit;
	private JButton forgotPW;
		
	/**
	 * constructor for LoginPanel.
	 */
	public LoginPanel() {
		this.setLayout(null);
		this.setBackground(Color.BLACK);
		
				
		jlogin = new JLabel("Login");
		jlogin.setBounds(350, 50, 400, 30);
		jlogin.setForeground(Color.green);
		jlogin.setFont(new Font("Arial", Font.BOLD, 28));		
		
		name = new JLabel("Username: ");
		name.setBounds(200, 200, 200, 30);
		name.setForeground(Color.GREEN);
		name.setFont(new Font("Arial", Font.PLAIN, 12)); 
		
		password = new JLabel("Password: ");
		password.setBounds(200, 300, 200, 25);
		password.setForeground(Color.green);
		password.setFont(new Font("Arial", Font.PLAIN, 12));
		
		lName = new JTextField();
		lName.setBounds(400, 200, 200, 25);
		lName.setFont(new Font("Arial", Font.PLAIN, 12));
		this.setGameScreenFocus(lName);
		
		lPassword = new JPasswordField();
		lPassword.setBounds(400, 300, 200, 25);
		lPassword.setEchoChar('*');
		lPassword.setFont(new Font("Arial", Font.PLAIN, 12));
		
		login = new JButton("Login");
		login.setBounds(200, 550, 200, 25);
		login.setBackground(Color.BLACK);
		login.setForeground(Color.GREEN);
		
		register = new JButton("Register");
		register.setBounds(500, 550, 200, 25);
		register.setBackground(Color.BLACK);
		register.setForeground(Color.GREEN);
		
		forgotPW = new JButton("Forgot Password?");
		forgotPW.setBounds(200, 600, 200, 25);
		forgotPW.setBackground(Color.BLACK);
		forgotPW.setForeground(Color.GREEN);
		
		
		exit = new JButton("Exit");
		exit.setBounds(500, 600, 200, 25);
		exit.setBackground(Color.BLACK);
		exit.setForeground(Color.GREEN);
		
		this.add(lName);	
		this.add(password);
		this.add(name);
		this.add(lPassword);
		this.add(login);
		this.add(register);
		this.add(jlogin);
		this.add(exit);
		this.add(forgotPW);
		lName.requestFocus();
				
		ActionListener a1 = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				loginProcess();
				}
			};
		login.addActionListener(a1);
		
		KeyAdapter loginKeyAdapter = new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() != KeyEvent.VK_ENTER) {
					return;
				}
				
				loginProcess();
			}
			
		};
		
		lPassword.addKeyListener(loginKeyAdapter);
		lName.addKeyListener(loginKeyAdapter);
		login.addKeyListener(loginKeyAdapter);
		
		ActionListener a2 = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				getCore().pushScreen(new RegisterPanel());
				}
			};
		register.addActionListener(a2);
		
		register.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				getCore().pushScreen(new RegisterPanel());
			}
			
		});
		
		ActionListener a3 = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				getCore().pushScreen(new forgotPWPanel());
				}
			};
		forgotPW.addActionListener(a3);
		
		forgotPW.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				getCore().pushScreen(new forgotPWPanel());
			}
			
		});
		
		ActionListener a4 = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				System.exit(0);
				}
			};
		exit.addActionListener(a4);
		
		exit.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				System.exit(0);
			}
			
		});
		
			
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void end() {
		getCore().getNetwork().removeListener(this);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void start() {
		getCore().getNetwork().addListener(this);
		this.login.setEnabled(true);
		this.lPassword.setText("");
		
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void update(ServerMessage m) {
		if (m instanceof LoginResponseMessage) {
			LoginResponseMessage response = (LoginResponseMessage) m;
			if (response.getResponseType() == IConstants.ResponseType.ok) {
				getCore().pushScreen(new GameLobby());
			} else if (response.getResponseType() == IConstants.ResponseType.version) {
				errorDialog("Wrong version \u2013 please download the latest version."
						+ "\n\n"
						+ "If that doesn't work, you may need to clear the Java WebStart cache.");
				login.setEnabled(true);
			} else {
				errorDialog("Login failed");
				login.setEnabled(true);
			}
		}
		
	}
	
	/**
	 * Dialog to show errors in the same colours than GUI.
	 * @param msg msg
	 */
	public void errorDialog(String msg) {
		UIManager.put("OptionPane.background", Color.BLACK);
		UIManager.put("OptionPane.JButton.setForground", Color.BLACK);
		UIManager.put("Panel.background", Color.BLACK);
		UIManager.put("OptionPane.messageForeground", Color.GREEN);
		JOptionPane.showMessageDialog(this, msg, "login error", 
				JOptionPane.ERROR_MESSAGE);
	}
	
	/**
	 * login.
	 */
	public void loginProcess() {
		getCore().getNetwork().makeContact();
		if (lName.getText().length() == 0 || String.valueOf(
				lPassword.getPassword()).length() == 0) {
		errorDialog("Login failed");
		return;
		}
		
		LoginRequestMessage loginMessage
		= new LoginRequestMessage();
		loginMessage.setVersion(Core.getVersion());
		loginMessage.setUsername(lName.getText());
		this.getCore().setPlayerName(lName.getText());
		loginMessage.setPassword(String.valueOf(
				lPassword.getPassword()));
		loginMessage.setMacaddress(getCore().getNetwork().getMACAddress());
		getCore().getNetwork().sendMessage(loginMessage);
		login.setEnabled(false);
	}
}




