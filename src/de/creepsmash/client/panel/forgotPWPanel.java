/**
 * 
 */
package de.creepsmash.client.panel;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.UIManager;

import de.creepsmash.client.network.MessageListener;
import de.creepsmash.common.IConstants;
import de.creepsmash.common.messages.client.PasswordResetRequestMessage;
import de.creepsmash.common.messages.server.LoginResponseMessage;
import de.creepsmash.common.messages.server.PasswordResetResponseMessage;
import de.creepsmash.common.messages.server.ServerMessage;

/**
 * LoginPanel at the beginning of the game.
 * 
 * @author sven
 * 
 */
public class forgotPWPanel extends GameScreen implements MessageListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JLabel name;
	private JLabel forgottenPW;
	private JLabel info;
	private JLabel info2;

	private JTextField lName;

	private JButton back;
	private JButton send;

	/**
	 * constructor for LoginPanel.
	 */
	public forgotPWPanel() {
		this.setLayout(null);
		this.setBackground(Color.BLACK);

		forgottenPW = new JLabel("Forgot your Password?");
		forgottenPW.setBounds(350, 50, 400, 30);
		forgottenPW.setForeground(Color.green);
		forgottenPW.setFont(new Font("Arial", Font.BOLD, 28));
		
		info = new JLabel("Please enter your username!");
		info.setBounds(400, 200, 200, 30);
		info.setForeground(Color.GREEN);
		info.setFont(new Font("Arial", Font.PLAIN, 14));

		info2 = new JLabel("If you didn't set your email address, this action won't work :-)!");
		info2.setBounds(300, 400, 400, 30);
		info2.setForeground(Color.GREEN);
		info2.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		
		name = new JLabel("Username: ");
		name.setBounds(200, 300, 200, 30);
		name.setForeground(Color.GREEN);
		name.setFont(new Font("Arial", Font.PLAIN, 12));

		lName = new JTextField();
		lName.setBounds(400, 300, 200, 25);
		lName.setFont(new Font("Arial", Font.PLAIN, 12));
		this.setGameScreenFocus(lName);

		send = new JButton("Send Password!");
		send.setBounds(200, 550, 200, 25);
		send.setBackground(Color.BLACK);
		send.setForeground(Color.GREEN);

		back = new JButton("Back");
		back.setBounds(500, 550, 200, 25);
		back.setBackground(Color.BLACK);
		back.setForeground(Color.GREEN);

		this.add(lName);
		this.add(name);
		this.add(forgottenPW);
		this.add(back);
		this.add(send);
		this.add(info);
		this.add(info2);
		lName.requestFocus();

		ActionListener a1 = new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				forgottenProcess();
			}
		};
		send.addActionListener(a1);

		KeyAdapter forgottenKeyAdapter = new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() != KeyEvent.VK_ENTER) {
					return;
				}

				forgottenProcess();
			}

		};

		ActionListener a2 = new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				getCore().popScreen();
			}
		};
		back.addActionListener(a2);

		back.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				getCore().popScreen();
			}

		});

		lName.addKeyListener(forgottenKeyAdapter);
		send.addKeyListener(forgottenKeyAdapter);
				
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void end() {
		getCore().getNetwork().removeListener(this);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void start() {
		getCore().getNetwork().addListener(this);
		this.send.setEnabled(true);

	}

	/**
	 * {@inheritDoc}
	 */
	public void update(ServerMessage m) {
		if (m instanceof LoginResponseMessage) {
			LoginResponseMessage response = (LoginResponseMessage) m;
			if (response.getResponseType() == IConstants.ResponseType.ok) {
				getCore().pushScreen(new GameLobby());
			} else {
				errorDialog("Login failed");
				send.setEnabled(true);
			}
		}
		
		if (m instanceof PasswordResetResponseMessage) {
			PasswordResetResponseMessage prrm = (PasswordResetResponseMessage) m;
			
			if (prrm.getResponseType() == IConstants.ResponseType.ok) {
				UIManager.put("OptionPane.background", Color.BLACK);
				UIManager.put("OptionPane.JButton.setForground", Color.BLACK);
				UIManager.put("Panel.background", Color.BLACK);
				UIManager.put("OptionPane.messageForeground", Color.GREEN);
				JOptionPane.showMessageDialog(this, "Sending a new Password was successful!", 
						"Check Mail!", JOptionPane.ERROR_MESSAGE);
			} 
			if (prrm.getResponseType() == IConstants.ResponseType.failed) {
				errorDialog("Sending a new password failed! \n\n" 
						+ "Perhaps you didn't set your email address or the typed " 
						+ "username doesn't exist?");
				this.setGameScreenFocus(lName);
			}
		}

	}

	/**
	 * Dialog to show errors in the same colours than GUI.
	 * 
	 * @param msg
	 *            msg
	 */
	public void errorDialog(String msg) {
		UIManager.put("OptionPane.background", Color.BLACK);
		UIManager.put("OptionPane.JButton.setForground", Color.BLACK);
		UIManager.put("Panel.background", Color.BLACK);
		UIManager.put("OptionPane.messageForeground", Color.GREEN);
		JOptionPane.showMessageDialog(this, msg, "error",
				JOptionPane.ERROR_MESSAGE);
	}

	/**
	 * login.
	 */
	public void forgottenProcess() {
		getCore().getNetwork().makeContact();
		
		PasswordResetRequestMessage prrm = new PasswordResetRequestMessage();
		prrm.setClientId(getCore().getPlayerId());
		prrm.setPlayerName(lName.getText());
		getCore().getNetwork().sendMessage(prrm);
		send.setEnabled(false);
		
	}
}
