/**
 Creep Smash, a multiplayer towerdefence game
 created as a project at the Hochschule fuer
 Technik Stuttgart (University of Applied Science)
 http://www.hft-stuttgart.de 

 Copyright (C) 2008 by      
 * Andreas Wittig
 * Bernd Hietler
 * Christoph Fritz
 * Fabian Kessel
 * Levin Fritz
 * Nikolaj Langner
 * Philipp Schulte-Hubbert
 * Robert Rapczynski
 * Ron Trautsch
 * Sven Supper
 http://creepsmash.sf.net/

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

package de.creepsmash.client.panel;

/**
 * 
 */

import java.awt.Color;
import java.awt.Dialog;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import de.creepsmash.client.network.MessageListener;
import de.creepsmash.common.IConstants;
import de.creepsmash.common.messages.client.CreateGameMessage;
import de.creepsmash.common.messages.server.CreateGameResponseMessage;
import de.creepsmash.common.messages.server.GameDescription;
import de.creepsmash.common.messages.server.ServerMessage;

/**
 * That JPanel class contains methods to create a new game.
 * 
 * @author Ron
 * 
 */
public class CreateGamePanel extends GameScreen implements MessageListener {

	/**
	 * @param args
	 */
	private static final long serialVersionUID = 1L;

	private JLabel game;
	private JLabel name;
	private JLabel player;
	private JLabel map;
	private JLabel Passwort;
	private JLabel MaxEloPoints;

	private JTextField tName;
	private JTextField tPasswort;
	private JComboBox tMaxEloPoints;
	private JComboBox tMinEloPoints;
	private JComboBox tPlayer;
	private JButton tMap;

	private JButton create;
	private JButton quit;

	private JLabel previewDescription;
	private ImageIcon preview;
	private JLabel previewLabel;
	
	private JFrame OpenCreateGameDialog = null;
	
	private int selectMap = 0;

	/**
	 * Creates a new instance of CreateGamePanel.
	 */
	public CreateGamePanel() {
		this.setLayout(null);
		this.setBackground(Color.BLACK);

		game = new JLabel("Create Game");
		game.setBounds(350, 50, 400, 30);
		game.setForeground(Color.green);
		game.setFont(new Font("Arial", Font.BOLD, 28));

		name = new JLabel("Name: ");
		name.setBounds(200, 200, 200, 30);
		name.setForeground(Color.GREEN);
		name.setFont(new Font("Arial", Font.PLAIN, 12));

		tName = new JTextField();
		tName.setText("Game of");
		tName.setBounds(300, 200, 200, 25);
		tName.setFont(new Font("Arial", Font.PLAIN, 12));
		this.setGameScreenFocus(tName);

		player = new JLabel("Players: ");
		player.setBounds(200, 250, 200, 25);
		player.setForeground(Color.green);
		player.setFont(new Font("Arial", Font.PLAIN, 12));

		tPlayer = new JComboBox();
		tPlayer.setBackground(Color.BLACK);
		tPlayer.setForeground(Color.GREEN);
		tPlayer.setBounds(300, 250, 200, 25);
		tPlayer.setFont(new Font("Arial", Font.PLAIN, 12));

		map = new JLabel("Map: ");
		map.setBounds(200, 300, 200, 25);
		map.setForeground(Color.green);
		map.setFont(new Font("Arial", Font.PLAIN, 12));

		tMap = new JButton("Select Map");
		tMap.setBackground(Color.BLACK);
		tMap.setForeground(Color.GREEN);
		tMap.setBounds(300, 300, 200, 25);

		Passwort = new JLabel("Password: ");
		Passwort.setBounds(200, 350, 200, 25);
		Passwort.setForeground(Color.green);
		Passwort.setFont(new Font("Arial", Font.PLAIN, 12));

		tPasswort = new JPasswordField();
		tPasswort.setBounds(300, 350, 200, 25);
		tPasswort.setFont(new Font("Arial", Font.PLAIN, 12));

		MaxEloPoints = new JLabel("min/max Points: ");
		MaxEloPoints.setBounds(200, 400, 200, 25);
		MaxEloPoints.setForeground(Color.green);
		MaxEloPoints.setFont(new Font("Arial", Font.PLAIN, 12));

		tMinEloPoints = new JComboBox();
		tMinEloPoints.setBackground(Color.BLACK);
		tMinEloPoints.setForeground(Color.GREEN);
		tMinEloPoints.setBounds(300, 400, 80, 25);
		tMinEloPoints.setFont(new Font("Arial", Font.PLAIN, 12));

		tMaxEloPoints = new JComboBox();
		tMaxEloPoints.setBackground(Color.BLACK);
		tMaxEloPoints.setForeground(Color.GREEN);
		tMaxEloPoints.setBounds(420, 400, 80, 25);
		tMaxEloPoints.setFont(new Font("Arial", Font.PLAIN, 12));

		create = new JButton("Create game");
		create.setBounds(200, 550, 200, 25);
		create.setBackground(Color.BLACK);
		create.setForeground(Color.GREEN);

		create = new JButton("Create game");
		create.setBounds(200, 550, 200, 25);
		create.setBackground(Color.BLACK);
		create.setForeground(Color.GREEN);

		quit = new JButton("Back");
		quit.setBounds(500, 550, 200, 25);
		quit.setBackground(Color.BLACK);
		quit.setForeground(Color.GREEN);

		previewDescription = new JLabel("preview");
		previewDescription.setBounds(600, 200, 250, 25);
		previewDescription.setHorizontalAlignment(SwingConstants.CENTER);
		previewDescription.setText("Random Map");
		previewDescription.setBackground(Color.BLACK);
		previewDescription.setForeground(Color.GREEN);

		String[] players = { "2 Players", "3 Players", "4 Players" };
		for (String s : players) {
			tPlayer.addItem(s);
		}

		
		try {
			preview = new ImageIcon(ImageIO.read(this.getClass()
					.getClassLoader().getResourceAsStream(
							"de/creepsmash/client/resources/maps/zufall.jpg")));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		preview.setImage(preview.getImage().getScaledInstance(250, 250,
				Image.SCALE_SMOOTH));
		previewLabel = new JLabel(preview);
		previewLabel.setBounds(600, 240, 250, 250);
		
		this.selectMap = 0;
		
		this.add(MaxEloPoints);
		this.add(tName);
		this.add(Passwort);
		this.add(tPasswort);
		this.add(tMaxEloPoints);
		this.add(tMinEloPoints);
		this.add(game);
		this.add(name);
		this.add(tName);
		this.add(player);
		this.add(tPlayer);
		this.add(map);
		this.add(tMap);
		this.add(create);
		this.add(quit);
		this.add(previewDescription);
		this.add(previewLabel);

		ActionListener a3 = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				openCreateGameDialog();
			}
		};
		
		this.tMap.addActionListener(a3);

		ActionListener a1 = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				getCore().popScreen();
				if (OpenCreateGameDialog.isVisible()) OpenCreateGameDialog.dispose();
			}
		};
		quit.addActionListener(a1);

		quit.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				getCore().popScreen();
				if (OpenCreateGameDialog.isVisible()) OpenCreateGameDialog.dispose();
			}
		});

		ActionListener a2 = new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				StartButton();
			}
		};
		create.addActionListener(a2);

		KeyAdapter createKeyAdapter = new KeyAdapter() {
			public void keyPressed(KeyEvent e) {

				if (e.getKeyCode() != KeyEvent.VK_ENTER) {
					return;
				}

				StartButton();
			}
		};

		create.addKeyListener(createKeyAdapter);
		tName.addKeyListener(createKeyAdapter);
		tPlayer.addKeyListener(createKeyAdapter);
		tMap.addKeyListener(createKeyAdapter);
		
	}
	private void openCreateGameDialog(){
		
		if (OpenCreateGameDialog == null){
			OpenCreateGameDialog = new CreateGameListPanel(this, "CreepSmash - Pleace select a map...");
		}
		
		OpenCreateGameDialog.setVisible(true);
		OpenCreateGameDialog.requestFocus();
	}
	private void StartButton() {

		boolean StartGame = false;

		

		if (tName.getText().length() != 0) {
			StartGame = true;
		} else {
			errorDialog("Please enter a name for the game!");
			StartGame = false;
		}
		if (tName.getText().length() > 12) {
			errorDialog("Maximum length of Gamename is 12");
			StartGame = false;
		}
		if (tMaxEloPoints.getSelectedIndex() == 0)
			tMaxEloPoints.setSelectedItem(0);
		if (tMinEloPoints.getSelectedIndex() == 0)
			tMinEloPoints.setSelectedItem(0);

		if (StartGame == true
				&& (tMaxEloPoints.getSelectedIndex() != 0 || tMinEloPoints
						.getSelectedIndex() != 0)) {

			if ((tMaxEloPoints.getSelectedIndex() < tMinEloPoints
					.getSelectedIndex() && tMaxEloPoints.getSelectedIndex() != 0)
					|| (tMaxEloPoints.getSelectedIndex() == tMinEloPoints
							.getSelectedIndex() && tMaxEloPoints
							.getSelectedIndex() != 0)) {

				errorDialog("'Max Elo Points' must be larger than 'min Elo Points'.!");
				StartGame = false;

			} else {
				StartGame = true;
			}
		}

		if (StartGame == true) {
			if (OpenCreateGameDialog != null){
				if (OpenCreateGameDialog.isVisible()) OpenCreateGameDialog.dispose();
			}
			getCore().setGamecreator(true);
			sendCreateGameMessage();

		}

	}

	/**
	 * Sends the Message to create a GameMessage.
	 */
	private void sendCreateGameMessage() {

		CreateGameMessage gM = new CreateGameMessage();
		gM.setGameName(tName.getText() + " (" + this.getCore().getPlayerName()
				+ ")");
		gM.setMapId(this.selectMap);
		gM.setMaxPlayers(tPlayer.getSelectedIndex() + 2);
		gM.setMaxEloPoints(Integer
				.valueOf(tMaxEloPoints.getSelectedIndex() * 100));
		gM.setMinEloPoints(Integer
				.valueOf(tMinEloPoints.getSelectedIndex() * 100));
		gM.setPasswort(tPasswort.getText());

		this.getCore().getNetwork().sendMessage(gM);
	}

	/**
	 * 
	 * @param g
	 *            Creates a new Game
	 */
	private void createGame(CreateGameResponseMessage g) {
		if (g.getResponseType().equals(IConstants.ResponseType.failed)) {
			errorDialog("Game already exists!");

		} else if (g.getResponseType().equals(IConstants.ResponseType.ok)) {
			GameDescription gd = new GameDescription();
			gd.setMaxPlayers(tPlayer.getSelectedIndex() + 2);
			gd.setMapId(this.selectMap);
			gd.setGameName(tName.getText());
			gd.setMaxEloPoints(Integer
					.valueOf(tMaxEloPoints.getSelectedIndex()) * 100);
			gd.setMinEloPoints(Integer
					.valueOf(tMinEloPoints.getSelectedIndex()) * 100);
			gd.setPasswort(tPasswort.getText());

			getCore().setActiveGame(gd);

			WaitingGamePanel wgp = new WaitingGamePanel();
			wgp.setMapId(this.selectMap);
			getCore().popScreen();
			getCore().pushScreen(wgp);

		}

	}

	/**
	 * @param msg
	 *            msg
	 */
	public void errorDialog(String msg) {
		UIManager.put("OptionPane.background", Color.BLACK);
		UIManager.put("Panel.background", Color.BLACK);
		UIManager.put("OptionPane.messageForeground", Color.GREEN);
		JOptionPane.showMessageDialog(this, msg, "error",
				JOptionPane.ERROR_MESSAGE);
	}

	public void selectMap(int id) {
		
		this.selectMap = id;
		
		try {
			preview = new ImageIcon(ImageIO.read(this.getClass()
					.getClassLoader().getResourceAsStream(
							IConstants.Map.getPicturePath(IConstants.Map.getMapById(id).toString()))));
			previewDescription.setText("Preview "
					+ IConstants.Map.getMapById(id).toString());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	
	preview.setImage(preview.getImage().getScaledInstance(250, 250,
			Image.SCALE_SMOOTH));
	previewLabel.setIcon(preview);
	
	OpenCreateGameDialog.setVisible(false);

	}

	/**
	 * displays the screen.
	 */
	@Override
	public void start() {
		this.getCore().getNetwork().makeContact();
		this.getCore().getNetwork().addListener(this);

		int playerScore = 0;
		playerScore = this.getCore().getPlayerEloScore();

		tMinEloPoints.addItem("all");
		tMaxEloPoints.addItem("all");

		for (int faktor = 100; faktor <= playerScore; faktor += 100) {

			tMinEloPoints.addItem(faktor);

		}

		if (playerScore == 0)
			playerScore = 5;
		for (int faktor = 100; faktor <= 10000; faktor += 100) {

			tMaxEloPoints.addItem(faktor);

		}

		this.repaint();
		
		

	}

	/**
	 * method for disappearing the screen.
	 */
	@Override
	public void end() {
		this.getCore().getNetwork().removeListener(this);
		// back to LobbyPanel
	}

	/**
	 * if mouse released, do run.
	 * 
	 * @param evt
	 *            for MouseEvent
	 */
	@Override
	public void mouseReleased(MouseEvent evt) {
		// TODO Auto-generated method stub

	}

	/**
	 * method for update.
	 * 
	 * @param m
	 *            for ServerMessage
	 */
	public void update(ServerMessage m) {
		if (m instanceof CreateGameResponseMessage) {
			createGame((CreateGameResponseMessage) m);
		}
	}

}
