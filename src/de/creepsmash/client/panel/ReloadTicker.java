
/**
   Creep Smash, a multiplayer towerdefence game
   created as a project at the Hochschule fuer
   Technik Stuttgart (University of Applied Science)
   http://www.hft-stuttgart.de 
   
   Copyright (C) 2008 by      
    * Andreas Wittig
    * Bernd Hietler
    * Christoph Fritz
    * Fabian Kessel
    * Levin Fritz
    * Nikolaj Langner
    * Philipp Schulte-Hubbert
    * Robert Rapczynski
    * Ron Trautsch
    * Sven Supper
    http://creepsmash.sf.net/

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/


package de.creepsmash.client.panel;

import javax.swing.JLabel;


/**
 * Thread to reload the Creep stack.
 * @author mi7hr4ndir
 *
 */
public class ReloadTicker extends Thread {
	
	

	private int[] creepCount;
	private int creepMax;
	private JLabel[] label;
	private int creepNumber;
	
	
	
	/**
	 * creates a new instance of ReloadTicer.
	 * @param creepNumber nuber of the creep
	 * @param creepCount creepCounter
	 * @param creepMax Max Creeps on the Stack
	 * @param label label to write how much creep are available
	 */
	public ReloadTicker(int creepNumber, int[] creepCount,
			int creepMax,  JLabel[] label) {
		this.creepNumber = creepNumber;
		this.creepCount = creepCount;
		this.creepMax = creepMax;
		this.label = label;
	}
	
	/**
	 * run method.
	 */
	public void run() {
		while (creepCount[creepNumber] < creepMax) {
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			creepCount[creepNumber]++;
//			label[creepNumber].setText("available: "
//					+ creepCount[creepNumber] + " / " 
//					+ creepMax);
			
		}
		this.interrupt();
	}

}
