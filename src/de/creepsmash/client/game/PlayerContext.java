
/**
   Creep Smash, a multiplayer towerdefence game
   created as a project at the Hochschule fuer
   Technik Stuttgart (University of Applied Science)
   http://www.hft-stuttgart.de 
   
   Copyright (C) 2008 by      
    * Andreas Wittig
    * Bernd Hietler
    * Christoph Fritz
    * Fabian Kessel
    * Levin Fritz
    * Nikolaj Langner
    * Philipp Schulte-Hubbert
    * Robert Rapczynski
    * Ron Trautsch
    * Sven Supper
    http://creepsmash.sf.net/

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/


package de.creepsmash.client.game;

import de.creepsmash.client.network.Network;
import de.creepsmash.client.sound.SoundManagement;
import de.creepsmash.common.IConstants;
import de.creepsmash.common.messages.client.GameOverMessage;



/**
 * The GameContext for the player.
 * @author Philipp
 *
 */
public class PlayerContext extends GameContext {

	private boolean deathMsgSent = false;
	
	/**
	 * Creates a new instance of PlayerContext.
	 * @param location the boardLocation (GameContext.BoardLocation)
	 * @param network the current network object
	 * @param managementSound Sound object for sound
	 * @param map the map
	 */
	public PlayerContext(BoardLocation location, Network network,
							SoundManagement managementSound, 
							IConstants.Map map) {
		super(location, network, managementSound, map);
		
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void postUpdate(long tickNumber) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void preUpdate(long tickNumber) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void sendDeathMessage() {
		if (!deathMsgSent) {
			System.out.println("death msg send for " + getPlayerName());
			
			GameOverMessage gom = new GameOverMessage();
			gom.setPosition(getWinningPosition());
			
			this.getNetwork().sendMessage(gom);
			deathMsgSent = true;
		}
		super.sendDeathMessage();
	}
	
	
}
