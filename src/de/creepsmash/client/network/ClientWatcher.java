
/**
   Creep Smash, a multiplayer towerdefence game
   created as a project at the Hochschule fuer
   Technik Stuttgart (University of Applied Science)
   http://www.hft-stuttgart.de 
   
   Copyright (C) 2008 by      
    * Andreas Wittig
    * Bernd Hietler
    * Christoph Fritz
    * Fabian Kessel
    * Levin Fritz
    * Nikolaj Langner
    * Philipp Schulte-Hubbert
    * Robert Rapczynski
    * Ron Trautsch
    * Sven Supper
    http://creepsmash.sf.net/

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/


package de.creepsmash.client.network;

import java.awt.Color;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Logger;

import javax.swing.JOptionPane;
import javax.swing.UIManager;

import de.creepsmash.client.panel.LoginPanel;
import de.creepsmash.common.messages.client.PongMessage;
import de.creepsmash.common.messages.server.GameMessage;
import de.creepsmash.common.messages.server.PingMessage;
import de.creepsmash.common.messages.server.ServerMessage;


/**
 * Watches for incomming messages form the Server.
 * @author fabian
 *
 */
public class ClientWatcher extends Thread {
	
	private static Logger logger = Logger.getLogger(
			ClientWatcher.class.getName());
	private Network network;
	private InTranslator inTrans;
	private InputStream in;
	
	private boolean interrupt = false;
	/**
	 * Creates a new instance of ClientWatcher.
	 * @param n Client
	 * @param in BufferedReader
	 */
	public ClientWatcher(Network n, InputStream in) {
		this.network = n;
		this.in = in;
		this.inTrans = new InTranslator(in);
	}
	
	/**
	 * run method.
	 */
	public void run() {
		while (!interrupt) {
			
			try {
				final ServerMessage m = inTrans.getNextMessage();
				
				if (m instanceof GameMessage) {
					network.addGameMessage((GameMessage) m);
				} else if (m instanceof PingMessage) {
					network.sendMessage(new PongMessage());
				} else {
					network.notifyListeners(m);
				}
				
			} catch (IOException e) {
				this.shutdown();
				logger.warning(e.getMessage());
				if (e.getMessage().contains("reset")) {
					errorHandling();
				}
			} catch (NullPointerException e) {
				this.shutdown();
				logger.info("Nullpointer detected");
				errorHandling();
			}
			
		}	
		try {
			in.close();
		} catch (IOException e) {
			logger.warning(e.getMessage());
		}
	}
	
	/**
	 * Stops reading from the server.
	 */
	public void shutdown() {
		this.interrupt = true;
	}
	
	/**
	 * Method to handle the error exceptions.
	 */
	public void errorHandling() {
		String[] options = { "Ok" };
		UIManager.put("OptionPane.background", Color.BLACK);
		UIManager.put("Panel.background", Color.BLACK);
		UIManager.put("OptionPane.messageForeground", Color.GREEN);
		int n = JOptionPane.showOptionDialog(null,
				"Connection aborted!", "Server error",
				JOptionPane.OK_OPTION, JOptionPane.INFORMATION_MESSAGE,
				null, options, options[0]);
		if (n == JOptionPane.OK_OPTION) {
									
			this.network.getCore().clearScreen();
			this.network.getCore().pushScreen(new LoginPanel());
			
		}
		System.out.println(n);
	}
}
