
/**
   Creep Smash, a multiplayer towerdefence game
   created as a project at the Hochschule fuer
   Technik Stuttgart (University of Applied Science)
   http://www.hft-stuttgart.de 
   
   Copyright (C) 2008 by      
    * Andreas Wittig
    * Bernd Hietler
    * Christoph Fritz
    * Fabian Kessel
    * Levin Fritz
    * Nikolaj Langner
    * Philipp Schulte-Hubbert
    * Robert Rapczynski
    * Ron Trautsch
    * Sven Supper
    http://creepsmash.sf.net/

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/


package de.creepsmash.client.network;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.logging.Logger;

import de.creepsmash.common.messages.server.BuildCreepRoundMessage;
import de.creepsmash.common.messages.server.BuildTowerRoundMessage;
import de.creepsmash.common.messages.server.ChangeStrategyRoundMessage;
import de.creepsmash.common.messages.server.CreateGameResponseMessage;
import de.creepsmash.common.messages.server.DeleteResponseMessage;
import de.creepsmash.common.messages.server.ErrorMessage;
import de.creepsmash.common.messages.server.GamesMessage;
import de.creepsmash.common.messages.server.HighscoreResponseMessage;
import de.creepsmash.common.messages.server.JoinGameResponseMessage;
import de.creepsmash.common.messages.server.KickPlayerResponseMessage;
import de.creepsmash.common.messages.server.KickedMessage;
import de.creepsmash.common.messages.server.LoginResponseMessage;
import de.creepsmash.common.messages.server.MessageMessage;
import de.creepsmash.common.messages.server.PasswordResetResponseMessage;
import de.creepsmash.common.messages.server.PingMessage;
import de.creepsmash.common.messages.server.PlayerJoinedMessage;
import de.creepsmash.common.messages.server.PlayerQuitMessage;
import de.creepsmash.common.messages.server.PlayersMessage;
import de.creepsmash.common.messages.server.RegistrationResponseMessage;
import de.creepsmash.common.messages.server.RoundMessage;
import de.creepsmash.common.messages.server.ScoreResponseMessage;
import de.creepsmash.common.messages.server.SellTowerRoundMessage;
import de.creepsmash.common.messages.server.ServerMessage;
import de.creepsmash.common.messages.server.StartGameMessage;
import de.creepsmash.common.messages.server.StartGameResponseMessage;
import de.creepsmash.common.messages.server.UpdateDataResponseMessage;
import de.creepsmash.common.messages.server.UpgradeTowerRoundMessage;

/**
 * The InTranslator translates incoming String-messages to message objects.
 * Regular Expressions are used for translating.
 * 
 * @author andreas
 * 
 */
public class InTranslator {

	private static Logger logger = Logger.getLogger(InTranslator.class
			.getName());

	private BufferedReader bufferedReader;
	private String messageString = new String("");

	/**
	 * @param inputStream
	 *            the InputStream from client-socket
	 */
	public InTranslator(InputStream inputStream) {
		super();
		this.bufferedReader = new BufferedReader(new InputStreamReader(
				inputStream, Charset.forName("UTF-8")));

	}

	/**
	 * @return the next message from input-stream (client-socket)
	 */
	public ServerMessage getNextMessage() throws IOException {
		ServerMessage messageObject = null;

		synchronized (bufferedReader) {
			messageString = "";
			messageString = this.bufferedReader.readLine();
		}

		if (messageString.length() < 1) {
			System.out.println(messageString);
			messageString = "I catched a verry silly bug!";
		}
		
		logger.info("MSG:" + messageString);

		if (BuildTowerRoundMessage.PATTERN.matcher(messageString).matches()) {
			messageObject = new BuildTowerRoundMessage();
		} else if (BuildCreepRoundMessage.PATTERN.matcher(messageString)
				.matches()) {
			messageObject = new BuildCreepRoundMessage();
		} else if (CreateGameResponseMessage.PATTERN.matcher(messageString)
				.matches()) {
			messageObject = new CreateGameResponseMessage();
		} else if (ErrorMessage.PATTERN.matcher(messageString).matches()) {
			messageObject = new ErrorMessage();
		} else if (GamesMessage.PATTERN.matcher(messageString).matches()) {
			messageObject = new GamesMessage();
		} else if (HighscoreResponseMessage.PATTERN.matcher(messageString)
				.matches()) {
			messageObject = new HighscoreResponseMessage();
		} else if (JoinGameResponseMessage.PATTERN.matcher(messageString)
				.matches()) {
			messageObject = new JoinGameResponseMessage();
		} else if (LoginResponseMessage.PATTERN.matcher(messageString)
				.matches()) {
			messageObject = new LoginResponseMessage();
		} else if (MessageMessage.PATTERN.matcher(messageString).matches()) {
			messageObject = new MessageMessage();
		} else if (PlayerJoinedMessage.PATTERN.matcher(messageString)
				.matches()) {
			messageObject = new PlayerJoinedMessage();
		} else if (PlayerQuitMessage.PATTERN.matcher(messageString)
				.matches()) {
			messageObject = new PlayerQuitMessage();
		} else if (PlayersMessage.PATTERN.matcher(messageString).matches()) {
			messageObject = new PlayersMessage();
		} else if (RegistrationResponseMessage.PATTERN.matcher(
				messageString).matches()) {
			messageObject = new RegistrationResponseMessage();
		} else if (RoundMessage.PATTERN.matcher(messageString).matches()) {
			messageObject = new RoundMessage();
		} else if (SellTowerRoundMessage.PATTERN.matcher(messageString)
				.matches()) {
			messageObject = new SellTowerRoundMessage();
		}else if (ChangeStrategyRoundMessage.PATTERN.matcher(messageString)
				.matches()) {
			messageObject = new ChangeStrategyRoundMessage();
		}else if (StartGameMessage.PATTERN.matcher(messageString)
				.matches()) {
			messageObject = new StartGameMessage();
		} else if (StartGameResponseMessage.PATTERN.matcher(messageString)
				.matches()) {
			messageObject = new StartGameResponseMessage();
		} else if (UpgradeTowerRoundMessage.PATTERN.matcher(messageString)
				.matches()) {
			messageObject = new UpgradeTowerRoundMessage();
		} else if (KickedMessage.PATTERN.matcher(messageString).matches()) {
			messageObject = new KickedMessage();
		} else if (KickPlayerResponseMessage.PATTERN.matcher(messageString)
				.matches()) {
			messageObject = new KickPlayerResponseMessage();
		} else if (PingMessage.PATTERN.matcher(messageString).matches()) {
			messageObject = new PingMessage();
		} else if (UpdateDataResponseMessage.PATTERN.matcher(messageString)
				.matches()) {
			messageObject = new UpdateDataResponseMessage();
		} else if (ScoreResponseMessage.PATTERN.matcher(messageString)
				.matches()) {
			messageObject = new ScoreResponseMessage();
		} else if (DeleteResponseMessage.PATTERN.matcher(messageString)
				.matches()) {
			messageObject = new DeleteResponseMessage();
		} else if (PasswordResetResponseMessage.PATTERN.matcher(messageString)
				.matches()) {
			messageObject = new PasswordResetResponseMessage();
		} else {
			// TODO logging
			System.err.println("Invalid message:\"" + messageString + "\"");
		}

		if (messageObject != null) {
			messageObject.initWithMessage(messageString);
			System.out.println(messageObject.getMessageString());
		}

		return messageObject;
	}
}
