
/**
   Creep Smash, a multiplayer towerdefence game
   created as a project at the Hochschule fuer
   Technik Stuttgart (University of Applied Science)
   http://www.hft-stuttgart.de 
   
   Copyright (C) 2008 by      
    * Andreas Wittig
    * Bernd Hietler
    * Christoph Fritz
    * Fabian Kessel
    * Levin Fritz
    * Nikolaj Langner
    * Philipp Schulte-Hubbert
    * Robert Rapczynski
    * Ron Trautsch
    * Sven Supper
    http://creepsmash.sf.net/

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/


package de.creepsmash.client.util;

import java.util.TimerTask;

import de.creepsmash.client.panel.GameLobby;
import de.creepsmash.common.messages.client.LogoutMessage;

/**
 * Class that executes a logout to the server when the
 * user is inaktive.
 * @author philipp
 *
 */
public class LobbyTimerTask extends TimerTask {

	private GameLobby lobby;
	
	/** Creates a new lobby timer.
	 * 
	 * @param lobby the actual gamelobby
	 */
	public LobbyTimerTask(GameLobby lobby) {
		this.lobby = lobby;
	}
	
	/**
	 * The run method that executes the logout.
	 */
	@Override
	public void run() {
		lobby.getCore().getNetwork().sendMessage(new LogoutMessage());
		lobby.getCore().getNetwork().shutdown();
		lobby.getCore().popScreen();
	}

}
