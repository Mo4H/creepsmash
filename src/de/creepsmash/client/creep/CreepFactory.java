
/**
   Creep Smash, a multiplayer towerdefence game
   created as a project at the Hochschule fuer
   Technik Stuttgart (University of Applied Science)
   http://www.hft-stuttgart.de 
   
   Copyright (C) 2008 by      
    * Andreas Wittig
    * Bernd Hietler
    * Christoph Fritz
    * Fabian Kessel
    * Levin Fritz
    * Nikolaj Langner
    * Philipp Schulte-Hubbert
    * Robert Rapczynski
    * Ron Trautsch
    * Sven Supper
    http://creepsmash.sf.net/

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/


package de.creepsmash.client.creep;

import de.creepsmash.client.game.GameContext;
import de.creepsmash.common.IConstants;
/**
 * Factory to create new creeps.
 * @author Philipp
 *
 */
public class CreepFactory {
	
	/**
	 * creates a Creep.
	 * @param context gets GameContext
	 * @param t type of creep
	 * @return type of creep
	 */
	public static Creep createCreep(GameContext context, IConstants.Creeps t) {
		switch(t) {
			case creep1:
				return new Creep1(context, IConstants.Creeps.creep1);
			case creep2:
				return new Creep2(context, IConstants.Creeps.creep2);
			case creep3:
				return new Creep3(context, IConstants.Creeps.creep3);
			case creep4:
				return new Creep4(context, IConstants.Creeps.creep4);
			case creep5:
				return new Creep5(context, IConstants.Creeps.creep5);
			case creep6:
				return new Creep6(context, IConstants.Creeps.creep6);
			case creep7:
				return new Creep7(context, IConstants.Creeps.creep7);
			case creep8:
				return new Creep8(context, IConstants.Creeps.creep8);
			case creep9:
				return new Creep9(context, IConstants.Creeps.creep9);
			case creep10:
				return new Creep10(context, IConstants.Creeps.creep10);
			case creep11:
				return new Creep11(context, IConstants.Creeps.creep11);
			case creep12:
				return new Creep12(context, IConstants.Creeps.creep12);
			case creep13:
				return new Creep13(context, IConstants.Creeps.creep13);
			case creep14:
				return new Creep14(context, IConstants.Creeps.creep14);
			case creep15:
				return new Creep15(context, IConstants.Creeps.creep15);
			case creep16:
				return new Creep16(context, IConstants.Creeps.creep16);
			default:
				return null;
		}
	}
}
