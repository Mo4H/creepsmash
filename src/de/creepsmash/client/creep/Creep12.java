
/**
   Creep Smash, a multiplayer towerdefence game
   created as a project at the Hochschule fuer
   Technik Stuttgart (University of Applied Science)
   http://www.hft-stuttgart.de 
   
   Copyright (C) 2008 by      
    * Andreas Wittig
    * Bernd Hietler
    * Christoph Fritz
    * Fabian Kessel
    * Levin Fritz
    * Nikolaj Langner
    * Philipp Schulte-Hubbert
    * Robert Rapczynski
    * Ron Trautsch
    * Sven Supper
    http://creepsmash.sf.net/

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/


/**
 * 
 */
package de.creepsmash.client.creep;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

import de.creepsmash.client.game.GameContext;
import de.creepsmash.client.util.Cache;
import de.creepsmash.common.IConstants;

/**
 * implementation of a Creep12.
 * @author sven
 *
 */
public class Creep12 extends AbstractCreep {
	
	
	/**
	 * constructor to set gamecontext and type of creep.
	 * @param context gamecontext
	 * @param t type of creep
	 */
	public Creep12(GameContext context, IConstants.Creeps t) {
		super(context, t);
		
		
		setImage(new BufferedImage(getWidth(), getHeight(),
				BufferedImage.TYPE_INT_ARGB));
		Graphics2D g2 = (Graphics2D) getImage().getGraphics();
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, 
				 RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setColor(Color.RED);
		g2.setStroke(new BasicStroke(1f));
		
		

		
		g2.drawOval(11, 10, 2, 2);
		g2.drawOval(7, 13, 2, 2);
		g2.drawOval(7, 7, 2, 2);
		
		g2.dispose();
		
	}
	
	/**
	 * Creates a new instance of DefaultCreep.
	 * @param context the game context
	 */
	//private BufferedImage image;
	
	
	/**
	 * {@inheritDoc}
	 */
	public String getDescription() {
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
	 * 
	 */
	public void loadImage() {
		
		if (Cache.getInstance().hasCreepImg(IConstants.Creeps.creep12)) {
			setImage(Cache.getInstance().getCreepImg(IConstants.Creeps.creep12));
		}
	}

}
