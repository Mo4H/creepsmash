package de.creepsmash.client.tower;

public class StrategyFactory {
	
	public static FindCreepStrategy getStrategyForName(String strategyName, Tower tower){

		if(strategyName.equals(FindClosestCreep.class.getSimpleName())){
			return new FindClosestCreep(tower);
		}else if(strategyName.equals(FindFarthestCreep.class.getSimpleName())){
			return new FindFarthestCreep(tower);
		}else if(strategyName.equals(FindFastestCreep.class.getSimpleName())){
			return new FindFastestCreep(tower);
		}else if(strategyName.equals(FindStrongestCreep.class.getSimpleName())){
			return new FindStrongestCreep(tower);
		}else if(strategyName.equals(FindWeakestCreep.class.getSimpleName())){
			return new FindWeakestCreep(tower);
		}else{
			return null;
		}
	}
}
