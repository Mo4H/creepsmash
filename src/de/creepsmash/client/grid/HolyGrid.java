
/**
   Creep Smash, a multiplayer towerdefence game
   created as a project at the Hochschule fuer
   Technik Stuttgart (University of Applied Science)
   http://www.hft-stuttgart.de 
   
   Copyright (C) 2008 by      
    * Andreas Wittig
    * Bernd Hietler
    * Christoph Fritz
    * Fabian Kessel
    * Levin Fritz
    * Nikolaj Langner
    * Philipp Schulte-Hubbert
    * Robert Rapczynski
    * Ron Trautsch
    * Sven Supper
    http://creepsmash.sf.net/

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/


package de.creepsmash.client.grid;

import java.awt.Graphics2D;

import de.creepsmash.client.game.GameContext;

/**
 * Just holy Grid in the map where noting
 * can be built.
 */
public class HolyGrid extends EmptyGrid {
	
	/**
	 * Creates a new instance of HolyGrid.
	 * @param x the x location
	 * @param y the y location
	 * @param context the gamecontext
	 */
	public HolyGrid(int x, int y, GameContext context) {
		super(x, y, context);
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean isFree() {
		// the HolyGrid is never free
		return false;
	}
	/**
	 * {@inheritDoc}
	 */
	public void paint(Graphics2D g) {
		// TODO Auto-generated method stub
		
	}
	/**
	 * {@inheritDoc}
	 */
	public void paintHighlight(Graphics2D g) {
		// TODO Auto-generated method stub
		
	}

}
