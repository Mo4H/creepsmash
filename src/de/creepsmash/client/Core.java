
/**
   Creep Smash, a multiplayer towerdefence game
   created as a project at the Hochschule fuer
   Technik Stuttgart (University of Applied Science)
   http://www.hft-stuttgart.de 
   
   Copyright (C) 2008 by      
    * Andreas Wittig
    * Bernd Hietler
    * Christoph Fritz
    * Fabian Kessel
    * Levin Fritz
    * Nikolaj Langner
    * Philipp Schulte-Hubbert
    * Robert Rapczynski
    * Ron Trautsch
    * Sven Supper
    http://creepsmash.sf.net/

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/


package de.creepsmash.client;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Stack;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.plaf.metal.MetalLookAndFeel;

import de.creepsmash.client.network.Network;
import de.creepsmash.client.panel.GameScreen;
import de.creepsmash.client.panel.StartPanel;
import de.creepsmash.client.sound.SoundManagement;
import de.creepsmash.common.IConstants;
import de.creepsmash.common.messages.server.GameDescription;



/**
 * The core of the game. This class manages the displayed screens and holds the
 * network connection.
 * 
 * @author Philipp
 * 
 */
public class Core extends JFrame {

	private static Logger logger = Logger.getLogger("de.creepsmash.client");

	public static final int HEIGHT = 700;
	public static final int WIDTH = 933;
	public static final Dimension SCREENSIZE = new Dimension(WIDTH, HEIGHT);

	private static final long serialVersionUID = 1L;
	private static String host;
	private static int port;
	private boolean gamecreator = false;

	private final Stack<GameScreen> screens = new Stack<GameScreen>();

	private final Network network;
	private String playerName;
	private Integer PlayerEloScore = 0;
	private Integer playerId;

	private GameDescription activeGame;

	private SoundManagement coreManagementSound;



	/**
	 * Creates a new core instance.
	 */
	public Core() {


		this.network = new Network(host, port, this);

		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				Core.this.network.shutdown();
			}
		});

		coreManagementSound = new SoundManagement();
	}

	/**
	 * Init the core.
	 */
	public void init() {
		this.pushScreen(new StartPanel(this));
	}

	/**
	 * Adds a screen to the top of the stack and makes it visible. If there is a
	 * screen on the stack, its end method is called.
	 * 
	 * @param screen
	 *            the game screen to add
	 */
	public void pushScreen(GameScreen screen) {

		if (!this.screens.isEmpty()) {
			this.screens.peek().end();
			this.remove(this.screens.peek());
		}

		screen.initialize(this);

		// set the bounds to let the screen know he should paint himself...
		screen.setBounds(0, 0, this.getWidth(), this.getHeight());

		this.add(screen);

		screen.requestFocusInWindow();

		this.screens.push(screen);

		screen.validate();
		screen.repaint();

		screen.start();
	}

	/**
	 * Shows the current Screen.
	 * 
	 * @return GameScreen current screen
	 */
	public GameScreen peekScreen() {
		return this.screens.peek();
	}

	/**
	 * Removes a screen from the stack.
	 */
	public void popScreen() {

		if (!this.screens.isEmpty()) {

			GameScreen popped = this.screens.peek();
			this.remove(this.screens.pop());

			this.add(this.screens.peek());
			this.screens.peek().setBounds(0, 0, this.getWidth(),
					this.getHeight());
			this.screens.peek().start();
			this.screens.peek().validate();
			this.screens.peek().initialize(this);

			popped.end();
		}

		this.repaint();
	}
    
	/**
	 * Clear the screen.
	 */
	public void clearScreen() {
		while (!this.screens.empty()) {
			GameScreen pop = this.screens.peek();
			this.remove(pop);
			pop.end();
			this.screens.pop();
		}
	}
	/**
	 * Getter for the network.
	 * 
	 * @return the network
	 */
	public Network getNetwork() {
		return this.network;
	}

	/**
	 * Getter for the Soundmanagement in the core.
	 * 
	 * @return SoundManagement
	 */
	public SoundManagement getCoreManagementSound() {
		return this.coreManagementSound;
	}

	/**
	 * Main method for the application.
	 * 
	 * @param args
	 *            the start parameters
	 */
	public static void main(String[] args) {
		if (args.length > 2) {
			System.err.println("wrong arguments");
			System.out.println("using default configuration...");
			host = IConstants.DEFAULT_SERVER_HOST;
			port = IConstants.DEFAULT_SERVER_PORT;
		} else if (args.length == 2) {
			host = args[0];
			port = Integer.parseInt(args[1]);
		} else if (args.length == 1) {
			host = args[0];
			port = IConstants.DEFAULT_SERVER_PORT;
		} else if (args.length == 0) {
			host = IConstants.DEFAULT_SERVER_HOST;
			port = IConstants.DEFAULT_SERVER_PORT;
		}

		try {
			UIManager.setLookAndFeel(new MetalLookAndFeel());
		} catch (UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Core core = new Core();
		core.setTitle("CreepSmash.de 0.5.4 beta ");
		core.getContentPane().setPreferredSize(Core.SCREENSIZE);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		core.setLocation((screenSize.width - Core.WIDTH)/2, (screenSize.height - Core.HEIGHT)/2);
		core.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		core.setResizable(false);
		core.pack();
		core.init();
		core.setVisible(true);
	}

	/**
	 * @return the gamecreator
	 */
	public boolean isGamecreator() {
		return this.gamecreator;
	}

	/**
	 * @param gamecreator
	 *            the gamecreator to set
	 */
	public void setGamecreator(boolean gamecreator) {
		this.gamecreator = gamecreator;
	}

	/**
	 * @return the playerName
	 */
	public String getPlayerName() {
		return this.playerName;
	}

	/**
	 * @param playerName
	 *            the playerName to set
	 */
	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}
	
	/**
	 * @return the getPlayerEloScore
	 */
	public int getPlayerEloScore() {
		return this.PlayerEloScore;
	}

	/**
	 * @param PlayerEloScore
	 *            the playerName to set
	 */
	public void setPlayerEloScore(int PlayerEloScore) {
		this.PlayerEloScore = PlayerEloScore;
	}
	
	/**
	 * @return the playerId
	 */
	public Integer getPlayerId() {
		return this.playerId;
	}

	/**
	 * @param playerId
	 *            the playerId to set
	 */
	public void setPlayerId(Integer playerId) {
		this.playerId = playerId;
	}

	/**
	 * @return the activeGame
	 */
	public GameDescription getActiveGame() {
		return activeGame;
	}

	/**
	 * @param activeGame
	 *            the activeGame to set
	 */
	public void setActiveGame(GameDescription activeGame) {
		this.activeGame = activeGame;
	}

	/**
	 * @return the version
	 */
	public static String getVersion() {
		String version = null;

		InputStream inStream = Core.class.getResourceAsStream("version");
		try {
			if (inStream.available() > 0) {
				InputStreamReader inStreamReader = null;

				try {
					inStreamReader = new InputStreamReader(inStream);
				} catch (Exception e) {
					logger.info("IOException: " + e);
				}

				try {
					BufferedReader reader =
						new BufferedReader(inStreamReader);
					version = reader.readLine();
				} catch (IOException e) {
					logger.info("IOException: " + e);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (version == null) {
			version = "-unknown-";
		}
		return version;
	}

}
