/**
   Creep Smash, a multiplayer towerdefence game
   created as a project at the Hochschule fuer
   Technik Stuttgart (University of Applied Science)
   http://www.hft-stuttgart.de 
   
   Copyright (C) 2008 by      
 * Andreas Wittig
 * Bernd Hietler
 * Christoph Fritz
 * Fabian Kessel
 * Levin Fritz
 * Nikolaj Langner
 * Philipp Schulte-Hubbert
 * Robert Rapczynski
 * Ron Trautsch
 * Sven Supper
    http://creepsmash.sf.net/

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

package de.creepsmash.common;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import de.creepsmash.client.creep.Creep.DamageType;

/**
 * Constants for client and server.
 * 
 * @author andreas
 */
public interface IConstants {

  /**
   * Default port for server-socket.
   */
  int DEFAULT_SERVER_PORT = 4747;

  /**
   * Default port for embedded webserver.
   */
  int DEFAULT_WEBSERVER_PORT = 0;

  /**
   * Default hostname for server (used for webstart).
   */
  String DEFAULT_HOSTNAME = "marissa.vdmastnet.nl";

  /**
   * Default hostname for server (used for webstart).
   */
  int DEFAULT_MAX_CLIENTS = 500;

  /**
   * Default Servername.
   */
  String DEFAULT_SERVER_HOST = "marissa.vdmastnet.nl";

  /**
   * How long is one tick (iteration through the game loop, in milliseconds)?
   */
  int TICK_MS = 50;
  /**
   * How many ticks are user actions (building/upgrading/selling a tower or
   * sending a creep) delayed?
   */
  int USER_ACTION_DELAY = 50;
  /**
   * Interval of income in Millis.
   */
  int INCOME_TIME = 15000;
  
  /**
   * Income at the beginning.
   */
  int START_INCOME = 200;

  /**
   * Credits at the beginning.
   */
  int CREDITS = 200;
  
  /**
   * MAX 2 same Ips in game.
   */
  boolean MUTIACCOUNT_IP_CHECK = false;
  
  /**
   * MAX 1 same MAC adress in game.
   */
  boolean MUTIACCOUNT_MAC_CHECK = true;
  
  /**
   * Lives at the beginning.
   */
  int LIVES = 20;

  double EAST = 0;
  double WEST = Math.PI;
  double WEST_MINUS = Math.PI * (-1);
  double SOUTH = (Math.PI / 2);
  double NORTH = (Math.PI / 2) * (-1);

  String SOUNDS_URL = "de/creepsmash/client/resources/sounds/";
  String SIMLEY_URL = "de/creepsmash/client/resources/smilies/";

  /**
   * Timeout used in the server. If the server receives no messages from a
   * client for TIMEOUT milliseconds, it sends PING. If it doesn't receive a
   * message for another TIMEOUT milliseconds after that, it disconnects the
   * client.
   */
  int TIMEOUT = 30 * 1000;

  /**
   * Creeps in einer Welle
   */
  long CREEPS_IN_WAVE = 20;
  /**
   * Zeitlicher Abstand zwischen 2 Creeps beim Senden einer Welle
   */
  long SEND_WAVE_DELAY = 130;
  /**
   * Zeitlicher Abstand zwischen 2 Creeps
   */
  long CREEP_DELAY = 130000000;
  /**
   * Zeitlicher Abstand zwischen 2 Wellen
   */
  long WAVE_DELAY = SEND_WAVE_DELAY * (CREEPS_IN_WAVE);

  /**
   * Describes a map for the game.
   */
    static enum Map {
    Random_Map("de/creepsmash/client/resources/maps/zufall.map"),
    REDWORLD("de/creepsmash/client/resources/maps/map_red.map"),
    ASSIALTA("de/creepsmash/client/resources/maps/map_assialta.map"),
    ASTEROID("de/creepsmash/client/resources/maps/map_asteroid.map"),
    BLACKHOLEVECTOR("de/creepsmash/client/resources/maps/map_blackholevector.map"),
    BLUE("de/creepsmash/client/resources/maps/map_blue.map"),
    BLUEMAGMA("de/creepsmash/client/resources/maps/map_bluemagma.map"),
    BLUESTARS("de/creepsmash/client/resources/maps/map_bluestars.map"),
    BLUEVECTORWATER("de/creepsmash/client/resources/maps/map_bluevectorwater.map"),
    CHESS("de/creepsmash/client/resources/maps/map_chess.map"),
    CHROMOSOMVEKTOR("de/creepsmash/client/resources/maps/map_chromosomvektor.map"),
    CIRCLE("de/creepsmash/client/resources/maps/map_circle.map"),
    CIRCLEVECTOR("de/creepsmash/client/resources/maps/map_circlevector.map"),
    COLOR("de/creepsmash/client/resources/maps/map_color.map"),
    COLORCIRCLEVECTOR("de/creepsmash/client/resources/maps/map_colorcirclevector.map"),
    CORRODED("de/creepsmash/client/resources/maps/map_corroded.map"),
    COSMOS("de/creepsmash/client/resources/maps/map_cosmos.map"),
    CROSSVECTOR("de/creepsmash/client/resources/maps/map_crossvector.map"),
    CROSSWATER("de/creepsmash/client/resources/maps/map_crosswater.map"),
    CRYSTAL("de/creepsmash/client/resources/maps/map_crystal.map"),
    DARKVECTOR("de/creepsmash/client/resources/maps/map_darkvector.map"),
    DESK("de/creepsmash/client/resources/maps/map_desk.map"),
    DISEASEDVECTOR("de/creepsmash/client/resources/maps/map_diseasedvector.map"),
    DOODLEWAR("de/creepsmash/client/resources/maps/map_doodlewar.map"),
    EASY("de/creepsmash/client/resources/maps/map_Easy.map"),
    EMERALDTILES("de/creepsmash/client/resources/maps/map_emeraldtiles.map"),
    FASTLANE("de/creepsmash/client/resources/maps/map_fastlane.map"),
    FLOWER("de/creepsmash/client/resources/maps/map_flower.map"),
    FLYINGANGEL("de/creepsmash/client/resources/maps/map_FlyingAngel.map"),
    FLYINGORB("de/creepsmash/client/resources/maps/map_flyingorb.map"),
    FROMSLOWTOFAST("de/creepsmash/client/resources/maps/map_fromslowtofast.map"),
    GOLDENAGE("de/creepsmash/client/resources/maps/map_goldenage.map"),
    GRAY("de/creepsmash/client/resources/maps/map_gray.map"),
    GREEN("de/creepsmash/client/resources/maps/map_green.map"),
    GREENCRISTALINE("de/creepsmash/client/resources/maps/map_GreenCristaline.map"),
    GREENIMBALANCE("de/creepsmash/client/resources/maps/map_greenimbalance.map"),
    GREENVECTOR("de/creepsmash/client/resources/maps/map_greenvector.map"),
    GRUNGE("de/creepsmash/client/resources/maps/map_grunge.map"),
    HANDY("de/creepsmash/client/resources/maps/map_handy.map"),
    JAPANVECTOR("de/creepsmash/client/resources/maps/map_japanvector.map"),
    JUMPINGCREEPS("de/creepsmash/client/resources/maps/map_jumpingcreeps.map"),
    JUNGLE("de/creepsmash/client/resources/maps/map_jungle.map"),
    KEEPOFFTHEGRASS("de/creepsmash/client/resources/maps/map_keepoffthegrass.map"),
    LAB("de/creepsmash/client/resources/maps/map_lab.map"),
    LADDERVECTOR("de/creepsmash/client/resources/maps/map_laddervector.map"),
    LAVA("de/creepsmash/client/resources/maps/map_lava.map"),
    LONG("de/creepsmash/client/resources/maps/map_long.map"),
    LOOPS("de/creepsmash/client/resources/maps/map_loops.map"),
    MAGICMIKE("de/creepsmash/client/resources/maps/map_magicmike.map"),
    MASTESOFCREEP("de/creepsmash/client/resources/maps/map_mastesofcreep.map"),
    MEADOW("de/creepsmash/client/resources/maps/map_meadow.map"),
    NOVA("de/creepsmash/client/resources/maps/map_nova.map"),
    NUCLEARVECTOR("de/creepsmash/client/resources/maps/map_nuclearvector.map"),
    ORANGEVECTOR("de/creepsmash/client/resources/maps/map_orangevector.map"),
    PATHOFDESTINY("de/creepsmash/client/resources/maps/map_pathofdestiny.map"),
    PINKVECTOR("de/creepsmash/client/resources/maps/map_pinkvector.map"),
    PLASMAVEKTOR("de/creepsmash/client/resources/maps/map_plasmavektor.map"),
    RACEWAYS("de/creepsmash/client/resources/maps/map_raceways.map"),
    RADIALFADE("de/creepsmash/client/resources/maps/map_radialfade.map"),
    RAINBOW("de/creepsmash/client/resources/maps/map_rainbow.map"),
    REDCOREWORLD("de/creepsmash/client/resources/maps/map_redcoreworld.map"),
    REDCREEP("de/creepsmash/client/resources/maps/map_redcreep.map"),
    REDVECTOR("de/creepsmash/client/resources/maps/map_redvector.map"),
    RICHTUNGSWECHSEL("de/creepsmash/client/resources/maps/map_richtungswechsel.map"),
    RUBYTILES("de/creepsmash/client/resources/maps/map_rubytiles.map"),
    SAPPHIRETILES("de/creepsmash/client/resources/maps/map_sapphiretiles.map"),
    SENFGLAS("de/creepsmash/client/resources/maps/map_senfglas.map"),
    SHOOPDAWHOOP("de/creepsmash/client/resources/maps/map_shoopdawhoop.map"),
    SPEEDRACE("de/creepsmash/client/resources/maps/map_speedrace.map"),
    SPEEDVECTOR("de/creepsmash/client/resources/maps/map_speedvector.map"),
    STAIRSVECTOR("de/creepsmash/client/resources/maps/map_stairsvector.map"),
    STARBASE("de/creepsmash/client/resources/maps/map_starbase.map"),
    STARS("de/creepsmash/client/resources/maps/map_stars.map"),
    STONES("de/creepsmash/client/resources/maps/map_stones.map"),
    STOP("de/creepsmash/client/resources/maps/map_stop.map"),
    SUMMER("de/creepsmash/client/resources/maps/map_summer.map"),
    SUMMER2("de/creepsmash/client/resources/maps/map_summer2.map"),
    SUN("de/creepsmash/client/resources/maps/map_sun.map"),
    TR2N("de/creepsmash/client/resources/maps/map_tr2n.map"),
    TRANSIT("de/creepsmash/client/resources/maps/map_Transit.map"),
    UPANDDOWN("de/creepsmash/client/resources/maps/map_upanddown.map"),
    VERLAUFEN("de/creepsmash/client/resources/maps/map_verlaufen.map"),
    VORTEX("de/creepsmash/client/resources/maps/map_vortex.map"),
    WALDGEIST("de/creepsmash/client/resources/maps/map_waldgeist.map"),
    WATERDROPS("de/creepsmash/client/resources/maps/map_waterdrops.map"),
    WHITEVECTOR("de/creepsmash/client/resources/maps/map_whitevector.map");



    private String filename = new String();

    /**
     * Constructor for enum.
     * 
     * @param flname
     *            Filename for Map
     */
    Map(String flname) {
      this.filename = flname;
    }

    /**
     * Filename of this Map.
     * 
     * @return String of Filename
     */
    public String getFilename() {
      return this.filename;
    }

    /**
     * Get a map by id.
     * 
     * @param id
     *            the id of the map
     * @return the map
     */
    public static Map getMapById(int id) {
      for (Map m : values()) {
        if (m.ordinal() == id) {
          return m;
        }
      }
      // default, if the id is wrong
      return REDWORLD;
    }

    /**
     * geter for the path of the picture of a map.
     * 
     * @param map
     *            mapname
     * @return picture path
     */
    public static String getPicturePath(String map) {

      String path = "zufall.jpg";
      String tempStr = null;
      InputStream res = (InputStream) IConstants.class.getClassLoader()
          .getResourceAsStream(Map.valueOf(map).getFilename());
      if (res == null) {
        System.out.println("Cannot open map: "
            + Map.valueOf(map).getFilename());

      } else {
        BufferedReader br = new BufferedReader(new InputStreamReader(
            res));

        try {
          while ((tempStr = br.readLine()) != null) {

            if (tempStr.contains(".bmp")
                || tempStr.contains(".png")
                || tempStr.contains(".jpg")) {
              path = tempStr.trim();
              break;
            }
          }
        } catch (IOException e) {

          System.out.println("Cannot read map: "
              + Map.valueOf(map).getFilename());

        }
      }

      return "de/creepsmash/client/resources/maps/" + path;

    }

    /**
     * geter for the path of the picture thumbnail of a map.
     * 
     * @param map
     *            mapname
     * @return picture path
     */
    public static String getPictureThumbnailPath(String map) {

      String path = "zufall.jpg";
      String tempStr = null;
      InputStream res = (InputStream) IConstants.class.getClassLoader()
          .getResourceAsStream(Map.valueOf(map).getFilename());
      if (res == null) {
        System.out.println("Cannot open map: "
            + Map.valueOf(map).getFilename());

      } else {
        BufferedReader br = new BufferedReader(new InputStreamReader(
            res));

        try {
          while ((tempStr = br.readLine()) != null) {

            if (tempStr.contains(".bmp")
                || tempStr.contains(".png")
                || tempStr.contains(".jpg")) {
              path = tempStr.trim();
              break;
            }
          }
        } catch (IOException e) {

          System.out.println("Cannot read map: "
              + Map.valueOf(map).getFilename());

        }
      }

      return "de/creepsmash/client/resources/maps/thumbnail/" + path;

    }
  }

  /**
   * Indicates the type of an error.
   * 
   * @author andreas
   * 
   */
  enum ErrorType {
    /**
     * Close Game/Client/Server after a fatal error.
     */
    Fatal,
    /**
     * Error, that can be handeld.
     */
    Error,
    /**
     * Just a warning, not really an error.
     */
    Warning
  }

  /**
   * Indicates the type of a response-message.
   * 
   * @author andreas
   * 
   */
  enum ResponseType {
    /**
     * Request was successful.
     */
    ok,
    /**
     * Request was unsuccessful.
     */
    failed,
    /**
     * Request failed because of the given username.
     */
    username,
    /**
     * Request failed because the client and server have different version.
     */
    version
  }

  /**
   * The type of the creep.
   */
  public static enum Creeps {
    // 10% income
    creep1(50, 5, 300, 70, 5, "Mercury", ""), creep2(100, 10, 700, 65, 10,
        "Mako", ""), creep3(250, 25, 1400, 80, 25, "Fast Nova", ""), creep4(
        500, 50, 3500, 50, 50, "Large Manta", ""),
    // 9% income
    creep5(1000, 90, 7000, 60, 90, "Demeter", ""), creep6(2000, 180, 14000,
        65, 180, "Ray", "Slow immunity"), creep7(4000, 360, 30000, 90,
        360, "Speedy Raider", "fast"), creep8(8000, 720, 80000, 60,
        720, "Big Toucan", ""),
    // 8% income
    creep9(15000, 1200, 140000, 70, 1200, "Vulture", ""), creep10(25000,
        2000, 250000, 75, 2000, "Shark", "Slow immunity"), creep11(
        40000, 3200, 500000, 100, 3200, "Racing Mamba", "fast"), creep12(
        60000, 4800, 1200000, 65, 4800, "Huge Titan", ""),
    // 7% income
    creep13(100000, 7000, 1500000, 65, 7000, "Zeus", "Regenerates"), creep14(
        200000, 14000, 2500000, 80, 14000, "Phoenix", "Slow immunity"), creep15(
        400000, 28000, 6000000, 140, 28000, "Express Raptor",
        "Super fast"), creep16(1000000, 56000, 15000000, 70, 56000,
        "Fat Colossus", "");

    private int price;
    private int income;
    private int health;
    private int speed;
    private int bounty;
    private String name;
    private String special;

    /**
     * @return the name
     */
    public String getName() {
      return name;
    }

    /**
     * @return the special
     */
    public String getSpecial() {
      return special;
    }

    /**
     * constructor for Enumeration.
     * 
     * @param price of creep
     * @param income for creep
     * @param health  of creep
     * @param speed of creep
     * @param bounty is the money you get for killing a creep
     */
    Creeps(int price, int income, int health, int speed, int bounty,
        String name, String special) {
      this.price = price;
      this.income = income;
      this.health = health;
      this.speed = speed;
      this.bounty = bounty;
      this.name = name;
      this.special = special;
    }

    /**
     * {@inheritDoc}
     */
    public int getPrice() {
      return price;
    }

    /**
     * {@inheritDoc}
     */
    public int getIncome() {
      return income;
    }

    /**
     * {@inheritDoc}
     */
    public int getHealth() {
      return health;
    }

    /**
     * {@inheritDoc}
     */
    public int getSpeed() {
      return speed;
    }

    /**
     * {@inheritDoc}
     */
    public String getSpeedString() {
      return translateSpeed(speed);
    }

    /**
     * {@inheritDoc}
     */
    public int getBounty() {
      return bounty;
    }

    /**
     * Translates the speedValue of the creep to a human readable
     * description.
     * 
     * @return the human readable string
     * @param value
     *            int value
     */
    public static String translateSpeed(int value) {
      String speed = "";
      if (value > 100) {
        speed = "ultra fast";
      } else if (value > 80) {
        speed = "very fast";
      } else if (value > 70) {
        speed = "fast";
      } else if (value > 65) {
        speed = "medium";
      } else if (value > 60) {
        speed = "slow";
      } else if (value > 55) {
        speed = "very slow";
      } else {
        speed = "ultra slow";
      }
      return speed;
    }
  }

  /**
   * The type of a tower.
   */
  public static enum Towers {
    // price | range | speed | damage | Sradius | Sreduction | slowRate %|
    // slowTime
    // | type | next | color

    tower13(3000, 50, 13, 1000, 0, 0.0, 0, 0, // 1538 dmg/s 4150
        DamageType.normal, null, Color.WHITE, "Basictower lvl 4", ""), tower12(
        1000, 45, 13, 250, 0, 0.0, 0, 0, // 384 dmg/s 1150
        DamageType.normal, tower13, Color.RED, "Basictower lvl 3", ""), tower11(
        100, 40, 13, 50, 0, 0.0, 0, 0, // 76 dmg/s 150
        DamageType.normal, tower12, Color.BLUE, "Basictower lvl 2", ""), tower1(
        50, 35, 13, 25, 0, 0.0, 0, 0, // 38 dmg/s 50
        DamageType.normal, tower11, Color.GREEN, "Basictower lvl 1", ""),

    tower23(3000, 50, 18, 100, 25, 0.7, 0.50,
        50, // 111 dmg/s 3700
        DamageType.slow, null, Color.WHITE, "Slowtower lvl 4",
        "slows multiple targets"), tower22(400, 50, 17, 75, 0, 0.0,
        0.45,
        50, // 88 dmg/s 700
        DamageType.slow, tower23, Color.RED, "Slowtower lvl 3",
        "slows target"), tower21(200, 45, 16, 50, 0, 0.0, 0.35,
        40, // 62 dmg/s 300
        DamageType.slow, tower22, Color.BLUE, "Slowtower lvl 2",
        "slows target"), tower2(100, 35, 15, 25, 0, 0.0, 0.30,
        40, // 33 dmg/s 100
        DamageType.slow, tower21, Color.GREEN, "Slowtower lvl 1",
        "slows target"),

    tower33(7500, 60, 10, 1100, 35, 0.5, 0,
        0, // 2200 dmg/s 11150
        DamageType.normal, null, Color.WHITE, "Splashtower lvl 4",
        "attacks multiple targets"), tower32(3000, 55, 10, 400, 35,
        0.6, 0,
        0, // 800 dmg/s 3650
        DamageType.normal, tower33, Color.RED, "Splashtower lvl 3",
        "attacks multiple targets"), tower31(750, 45, 12, 200, 35, 0.7,
        0,
        0, // 333 dmg/s 1000
        DamageType.normal, tower32, Color.BLUE, "Splashtower lvl 2",
        "attacks multiple targets"), tower3(250, 40, 15, 50, 35, 0.7,
        0,
        0, // 66 dmg/s 250
        DamageType.normal, tower31, Color.GREEN, "Splashtower lvl 1",
        "attacks multiple targets"),

    tower43(15000, 80, 60, 15000, 35, 0.6, 0,
        0, // 5000 dmg/s 26500
        DamageType.normal, null, Color.WHITE, "Rockettower lvl 4",
        "attacks multiple targets"), tower42(7500, 70, 65, 7500, 30,
        0.7, 0,
        0, // 2307 dmg/s 11500
        DamageType.normal, tower43, Color.RED, "Rockettower lvl 3",
        "attacks multiple targets"), tower41(3000, 60, 75, 2500, 25,
        0.8, 0,
        0, // 666 dmg/s 4000
        DamageType.normal, tower42, Color.BLUE, "Rockettower lvl 2",
        "attacks multiple targets"), tower4(1000, 50, 75, 1000, 25,
        0.8, 0,
        0, // 266 dmg/s 1000
        DamageType.normal, tower41, Color.GREEN, "Rockettower lvl 1",
        "attacks multiple targets"),

    tower53(15000, 65, 3, 1800, 0, 0.0, 0, 0, // 12000 dmg/s 26500
        DamageType.normal, null, Color.WHITE, "Speedtower lvl 4", ""), tower52(
        7500, 60, 5, 1100, 0, 0.0, 0, 0, // 4400 dmg/s 11500
        DamageType.normal, tower53, Color.RED, "Speedtower lvl 3", ""), tower51(
        3000, 55, 7, 450, 0, 0.0, 0, 0, // 1285 dmg/s 4000
        DamageType.normal, tower52, Color.BLUE, "Speedtower lvl 2", ""), tower5(
        1000, 50, 9, 225, 0, 0.0, 0, 0, // 500 dmg/s 1000
        DamageType.normal, tower51, Color.GREEN, "Speedtower lvl 1", ""),

    tower61(50000, 150, 50, 40000, 0, 0.0, 0, 0, // 16000 dam/s 70000
        DamageType.normal, null, Color.WHITE, "Ultimatetower lvl 2", ""), tower6(
        20000, 100, 100, 25000, 0, 0.0, 0,
        0, // 5000 dmg/s 20000
        DamageType.normal, tower61, Color.GREEN, "Ultimatetower lvl 1",
        "");

    private int price;
    private float range;
    private int speed;
    private int damage;
    private int splashRadius;
    private double damageReductionAtRadius;
    private double slowRate;
    private int slowTime;
    private DamageType damageType;
    private IConstants.Towers next;
    private Color towerColor;
    private String name;
    private String special;

    /**
     * @return the name
     */
    public String getName() {
      return name;
    }

    /**
     * @return the special
     */
    public String getSpecial() {
      return special;
    }

    /**
     * @return the slowTime
     */
    public int getSlowTime() {
      return slowTime;
    }

    /**
     * @return the splashRadius
     */
    public int getSplashRadius() {
      return splashRadius;
    }

    /**
     * @return the damageReductionAtRadius
     */
    public double getDamageReductionAtRadius() {
      return damageReductionAtRadius;
    }

    /**
     * @return the slowRate
     */
    public double getSlowRate() {
      return slowRate;
    }

    /**
     * @return the damageType
     */
    public DamageType getDamageType() {
      return damageType;
    }

    /**
     * @param damageType
     *            the damageType to set
     */
    public void setDamageType(DamageType damageType) {
      this.damageType = damageType;
    }

    /**
     * specifies Type of tower.
     * 
     * @param price
     *            of tower, on upgrades the upgrade price
     * @param range
     *            of tower, on upgrades the new range
     * @param speed
     *            of tower, on upgrades the new speed
     * @param damage
     *            of tower, on upgrades the new damage
     * @param spashRadius
     *            of the tower, on upgrades the new radius
     * @param damageReductionAtRadius
     *            of the tower, on upgrades the new one
     * @param slowRate
     *            of the tower in %, on upgrades the new slowRate
     * @param slowTime
     *            of a slowed Creep in ticks, on upgrades the new time
     * @param damageType
     *            of the tower, on upgrades the new one
     * @param next
     *            next upgrade type
     * @param towerColor
     *            color of the tower
     * @param name
     *            of the tower
     * @param special
     *            of the tower
     */

    Towers(int price, int range, int speed, int damage, int spashRadius,
        double damageReductionAtRadius, double slowRate, int slowTime,
        DamageType damageType, IConstants.Towers next,
        Color towerColor, String name, String special) {

      this.price = price;
      this.range = range;
      this.speed = speed;
      this.damage = damage;
      this.splashRadius = spashRadius;
      this.damageReductionAtRadius = damageReductionAtRadius;
      this.slowRate = slowRate;
      this.slowTime = slowTime;
      this.damageType = damageType;
      this.next = next;
      this.towerColor = towerColor;
      this.name = name;
      this.special = special;

    }

    /**
     * getter for price of tower.
     * 
     * @return priece
     */
    public int getPrice() {
      return price;
    }

    /**
     * getter for range of tower.
     * 
     * @return range
     */
    public float getRange() {
      return range;
    }

    /**
     * getter for speed of tower.
     * 
     * @return speed
     */
    public int getSpeed() {
      return speed;
    }

    /**
     * getter.
     * 
     * @return the speed of the tower as a string
     */
    public String getSpeedString() {
      return translateSpeed(speed);
    }

    /**
     * getter for damage of tower.
     * 
     * @return damage
     */
    public int getDamage() {
      return damage;
    }

    /**
     * getter.
     * 
     * @return next Towertype after upgrade
     */
    public IConstants.Towers getNext() {
      return next;
    }

    /**
     * @return the towerColor
     */
    public Color getTowerColor() {
      return towerColor;
    }

    /**
     * Translates the speedValue of the tower to a human readable
     * description.
     * 
     * @return the human readable string
     * @param value
     *            int value
     */
    public static String translateSpeed(int value) {
      String speed = "";
      if (value > 50) {
        speed = "ultra slow";
      } else if (value > 20) {
        speed = "very slow";
      } else if (value > 15) {
        speed = "slow";
      } else if (value > 10) {
        speed = "medium";
      } else if (value > 7) {
        speed = "fast";
      } else if (value > 3) {
        speed = "very fast";
      } else {
        speed = "ultra fast";
      }
      return speed;
    }
  }

}
