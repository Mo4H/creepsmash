
/**
   Creep Smash, a multiplayer towerdefence game
   created as a project at the Hochschule fuer
   Technik Stuttgart (University of Applied Science)
   http://www.hft-stuttgart.de 
   
   Copyright (C) 2008 by      
    * Andreas Wittig
    * Bernd Hietler
    * Christoph Fritz
    * Fabian Kessel
    * Levin Fritz
    * Nikolaj Langner
    * Philipp Schulte-Hubbert
    * Robert Rapczynski
    * Ron Trautsch
    * Sven Supper
    http://creepsmash.sf.net/

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/


package de.creepsmash.common.messages.server;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.creepsmash.common.messages.MessageUtil;

/**
 * Description of a game. Part of GamesMessage.
 * 
 * @author andreas
 *
 */
public class GameDescription {
	
	private Integer gameId;
	private String gameName;
	private Integer mapId;
	private Integer maxPlayers;
	private Integer currentPlayers;
	private String Passwort;
	private String Player1;
	private String Player2;
	private String Player3;
	private String Player4;
	private Integer MaxEloPoints;
	private Integer MinEloPoints;
	private String state;

	private static final String REG_EXP = "(\\s)*([0-9]+)\\s\"([^\"]+)\"\\s" 
			+ "([0-9]+)\\s([0-9]+)\\s([0-9]+)\\s([0-9]+)\\s([0-9]+)"
			+ "\\s\"([^\"]+)\"\\s\"(.*)\"\\s\"(.*)\"\\s\"(.*)\"\\s\"(.*)\"\\s\"([^\"]+)\"(\\s)*";
	
	public static final Pattern PATTERN = Pattern.compile(REG_EXP);
	
	/**
	 * default constructor.
	 */
	public GameDescription() {
		super();
	}
	
	/**
	 * @param gameId the id of the game
	 * @param gameName the name of the game
	 * @param mapId the id of the map
	 * @param maxPlayers the number of max players
	 * @param currentPlayers the number of current players
	 * @param state the state of the game
	 */
	public GameDescription(Integer gameId, String gameName, Integer mapId,
			Integer maxPlayers, Integer currentPlayers, Integer MaxEloPoints, Integer MinEloPoints, String Passwort,
			String Player1,
			String Player2,
			String Player3,
			String Player4,
			String state) {
		super();
		this.gameId = gameId;
		this.gameName = gameName;
		this.mapId = mapId;
		this.maxPlayers = maxPlayers;
		this.currentPlayers = currentPlayers;
		this.MaxEloPoints = MaxEloPoints;
		this.MinEloPoints = MinEloPoints;
		this.Passwort = Passwort;
		this.Player1 = Player1;
		this.Player2 = Player2;
		this.Player3 = Player3;
		this.Player4 = Player4;
		this.state = state;
		
	}

	/**
	 * @return the gameId
	 */
	public Integer getGameId() {
		return this.gameId;
	}

	/**
	 * @param gameId the gameId to set
	 */
	public void setGameId(Integer gameId) {
		this.gameId = gameId;
	}

	/**
	 * @return the gameName
	 */
	public String getGameName() {
		return this.gameName;
	}

	/**
	 * @param gameName the gameName to set
	 */
	public void setGameName(String gameName) {
		this.gameName = gameName;
	}

	/**
	 * @return the mapId
	 */
	public Integer getMapId() {
		return this.mapId;
	}

	/**
	 * @param mapId the mapId to set
	 */
	public void setMapId(Integer mapId) {
		this.mapId = mapId;
	}
	
	
	/**
	 * @return the Plyer1
	 */
	public String getPlayer1() {
		return this.Player1;
	}

	/**
	 * @param  Player to set Player1
	 */
	public void setPlyer1(String Player) {
		this.Player1 = Player;
	}
	
	/**
	 * @return the Plyer2
	 */
	public String getPlayer2() {
		return this.Player2;
	}

	/**
	 * @param  Player to set Player2
	 */
	public void setPlyer2(String Player) {
		this.Player2 = Player;
	}
	
	/**
	 * @return the Plyer3
	 */
	public String getPlayer3() {
		return this.Player3;
	}

	/**
	 * @param  Player to set Player3
	 */
	public void setPlyer3(String Player) {
		this.Player3 = Player;
	}
	
	/**
	 * @return the Plyer4
	 */
	public String getPlayer4() {
		return this.Player4;
	}

	/**
	 * @param  Player to set Player4
	 */
	public void setPlyer4(String Player) {
		this.Player4 = Player;
	}
	
	/**
	 * @return the maxPlayers
	 */
	public Integer getMaxPlayers() {
		return this.maxPlayers;
	}

	/**
	 * @param maxPlayers the maxPlayers to set
	 */
	public void setMaxPlayers(Integer maxPlayers) {
		this.maxPlayers = maxPlayers;
	}

	/**
	 * @return the currentPlayers
	 */
	public Integer getCurrentPlayers() {
		return this.currentPlayers;
	}

	/**
	 * @param currentPlayers the currentPlayers to set
	 */
	public void setCurrentPlayers(Integer currentPlayers) {
		this.currentPlayers = currentPlayers;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return this.state;
	}
	
	/**
	 * @param Passwort the Passwort of this game
	 */
	public void setPasswort(String Passwort) {
		this.Passwort = Passwort;		
	}
	
	/**
	 * @return Game Passwort
	 */
	public String getPasswort() {
		return this.Passwort;
	}
	
	/**
	 * @param MaxEloPoints of this game
	 */
	public void setMaxEloPoints(Integer MaxEloPoints) {
		this.MaxEloPoints = MaxEloPoints;		
	}
	
	/**
	 * @return the number of MaxEloPoints
	 */
	public Integer getMaxEloPoints() {
		return this.MaxEloPoints;
	}
	
	/**
	 * @param maxPlayers MinEloPoints of this game
	 */
	public void setMinEloPoints(Integer MinEloPoints) {
		this.MinEloPoints = MinEloPoints;		
	}
	
	/**
	 * @return the number of MinEloPoints
	 */
	public Integer getMinEloPoints() {
		return this.MinEloPoints;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}
	
	/**
	 * @return the String representation of the message.
	 */
	@Override
	public String toString() {
		return this.gameId.toString() + " " 
		+ "\"" + this.gameName + "\" " + this.mapId.toString() + " " 
		+ this.maxPlayers.toString() + " " + this.currentPlayers
		+ " " + this.getMaxEloPoints() + " " + this.getMinEloPoints() + " "
		+ "\"" +MessageUtil.prepareToSend(this.getPasswort()) + "\" "
		+ "\"" +MessageUtil.prepareToSend(this.getPlayer1()) + "\" "
		+ "\"" +MessageUtil.prepareToSend(this.getPlayer2()) + "\" "
		+ "\"" +MessageUtil.prepareToSend(this.getPlayer3()) + "\" "
		+ "\"" +MessageUtil.prepareToSend(this.getPlayer4()) + "\" "
		+ "\"" + this.state + "\"";
	}
	
	
	/**
	 * @param messageString the messageString
	 */
	public void initWithMessage(String messageString) {
		Matcher matcher = PATTERN.matcher(messageString);
		if (matcher.matches()) {
			this.setGameId(Integer.valueOf(matcher.group(2)));
			this.setGameName(matcher.group(3));
			this.setMapId(Integer.valueOf(matcher.group(4)));
			this.setMaxPlayers(Integer.valueOf(matcher.group(5)));
			this.setCurrentPlayers(Integer.valueOf(matcher.group(6)));
			this.setMaxEloPoints(Integer.valueOf(matcher.group(7)));
			this.setMinEloPoints(Integer.valueOf(matcher.group(8)));
			this.setPasswort(matcher.group(9));
			this.setPlyer1(matcher.group(10));
			this.setPlyer2(matcher.group(11));
			this.setPlyer3(matcher.group(12));
			this.setPlyer4(matcher.group(13));
			this.setState(matcher.group(14));
		}
	}
		
	/**
	 * Returns true if o is a GameDescription instance with all fields equal
	 * to this object's.
	 * @param o the object to compare to.
	 * @return true if o is equal to this object.
	 */
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof GameDescription)) {
			return false;
		}
		GameDescription d = (GameDescription) o;
		return this.gameId == d.getGameId()
			&& this.gameName.equals(d.getGameName())
			&& this.mapId == d.getMapId()
			&& this.maxPlayers == d.getMaxPlayers()
			&& this.currentPlayers == d.getCurrentPlayers()
			&& this.MaxEloPoints == d.getMaxEloPoints()
			&& this.MinEloPoints == d.getMinEloPoints()
			&& this.Passwort.equals(d.getPasswort())
			&& this.Player1.equals(d.getPlayer1())
			&& this.Player2.equals(d.getPlayer2())
			&& this.Player3.equals(d.getPlayer3())
			&& this.Player4.equals(d.getPlayer4())
			&& this.state.equals(d.getState());
	}

	/**
	 * Returns a hash code for this object.
	 * @return a hash code
	 */
	@Override
	public int hashCode() {
		return this.gameId;
	}

}
