
/**
   Creep Smash, a multiplayer towerdefence game
   created as a project at the Hochschule fuer
   Technik Stuttgart (University of Applied Science)
   http://www.hft-stuttgart.de 
   
   Copyright (C) 2008 by      
    * Andreas Wittig
    * Bernd Hietler
    * Christoph Fritz
    * Fabian Kessel
    * Levin Fritz
    * Nikolaj Langner
    * Philipp Schulte-Hubbert
    * Robert Rapczynski
    * Ron Trautsch
    * Sven Supper
    http://creepsmash.sf.net/

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/


package de.creepsmash.common.messages.client;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.creepsmash.common.messages.MessageUtil;

/**
 * Message from client, building a creep.
 * 
 * @author andreas
 *
 */
public class BuildCreepMessage extends ClientMessage implements GameMessage {

	/**
	* Regular expression for message-parsing.
	*/
	private static final String REGEXP_BUILD_CREEP =
		"BUILD_CREEP\\s\"([^\"]+)\"";
	
	/**
	 * Pattern for regular expression.
	 */
	public static final Pattern PATTERN = Pattern.compile(REGEXP_BUILD_CREEP);
		
	
	private String creepType;

	/**
	 * @return the type of the creep
	 */
	public String getCreepType() {
		return this.creepType;
	}

	/**
	 * @param creepType the type of the creep
	 */
	public void setCreepType(String creepType) {
		this.creepType = creepType;
	}

	/**
	 * @param messageString the message as String.
	 */
	@Override
	public void initWithMessage(String messageString) {
		Matcher matcher = PATTERN.matcher(messageString);
		if (matcher.matches()) {
			this.setCreepType(matcher.group(1));
		}
	}

	/**
	 * @return the message as String.
	 */
	@Override
	public String getMessageString() {
		return "BUILD_CREEP \"" 
		+ MessageUtil.prepareToSend(this.getCreepType()) + "\"";
	}
	
	
		
}
