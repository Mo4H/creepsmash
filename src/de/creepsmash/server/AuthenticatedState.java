
/**
   Creep Smash, a multiplayer towerdefence game
   created as a project at the Hochschule fuer
   Technik Stuttgart (University of Applied Science)
   http://www.hft-stuttgart.de 
   
   Copyright (C) 2008 by      
    * Andreas Wittig
    * Bernd Hietler
    * Christoph Fritz
    * Fabian Kessel
    * Levin Fritz
    * Nikolaj Langner
    * Philipp Schulte-Hubbert
    * Robert Rapczynski
    * Ron Trautsch
    * Sven Supper
    http://creepsmash.sf.net/

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/


package de.creepsmash.server;

import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;

import de.creepsmash.common.IConstants;
import de.creepsmash.common.messages.client.ClientMessage;
import de.creepsmash.common.messages.client.CreateGameMessage;
import de.creepsmash.common.messages.client.DeleteRequestMessage;
import de.creepsmash.common.messages.client.HighscoreRequestMessage;
import de.creepsmash.common.messages.client.JoinGameRequestMessage;
import de.creepsmash.common.messages.client.LogoutMessage;
import de.creepsmash.common.messages.client.RefreshMessage;
import de.creepsmash.common.messages.client.ScoreRequestMessage;
import de.creepsmash.common.messages.client.SendMessageMessage;
import de.creepsmash.common.messages.client.UpdateDataRequestMessage;
import de.creepsmash.common.messages.server.DeleteResponseMessage;
import de.creepsmash.common.messages.server.MessageMessage;
import de.creepsmash.common.messages.server.ScoreResponseMessage;
import de.creepsmash.common.messages.server.ServerMessage;
import de.creepsmash.common.messages.server.UpdateDataResponseMessage;
import de.creepsmash.server.game.Game;
import de.creepsmash.server.model.Player;

/**
 * This class represents the state of the client when it is logged in.
 * 
 * @author Bernd Hietler
 */
public class AuthenticatedState extends ClientState {

	private static Logger authenticatedStateLogger = Logger
			.getLogger(AuthenticatedState.class);
	private Lobby lobby;
	private AnonymousState anonymousState;

	/**
	 * Constructor.
	 * 
	 * @param outQueue BlockingQueue for the outgoing messages.
	 * @param client Client
	 * @param lobby Lobby
	 * @param anonymousState the previous state.
	 * @param authenticationService the AuthenticationService.
	 */
	public AuthenticatedState(
			BlockingQueue<QueueMessage<ServerMessage>> outQueue, Client client,
			Lobby lobby, AnonymousState anonymousState,
			AuthenticationService authenticationService) {
		super(outQueue, client, authenticationService);
		this.lobby = lobby;
		this.lobby.newClient(client);
		this.anonymousState = anonymousState;
	}

	/**
	 * Method for sending a Message.
	 * 
	 * @param message
	 *            ClientMessage
	 * @return ClientState
	 */
	@Override
	public ClientState receiveMessage(ClientMessage message) {
		if (message == null) {
			this.lobby.leaveLobby(this.getClient());
			this.logout();
			authenticatedStateLogger.info(
					"client " + this.getClient() 
					+ " disconnected in AuthenticatedState");
			return null;
		} else if (message instanceof UpdateDataRequestMessage) {
			UpdateDataRequestMessage updateDataRequestMessage = 
				(UpdateDataRequestMessage) message;
						
			UpdateDataResponseMessage m = new UpdateDataResponseMessage();
			m.setResponseType(AuthenticationService.update(
					this.getClient().getUserName(), 
					updateDataRequestMessage.getOldPassword(), 
					updateDataRequestMessage.getPassword(), 
					updateDataRequestMessage.getEmail()));
			this.getClient().send(m);
			return this;
		} else if (message instanceof DeleteRequestMessage) {
			DeleteResponseMessage m = new DeleteResponseMessage();
			m.setResponseType(AuthenticationService.delete(
					this.getClient()
					.getUserName()));
			this.getClient().send(m);
			return this;
		} else if (message instanceof SendMessageMessage) {
			
			boolean sendMSG = true;

			String[] msgSplit = ((SendMessageMessage) message).getMessage()
					.split(" ");

			if (msgSplit.length >= 1) {
				// Kick User Modulo Permission = 2
				if (msgSplit[0].equalsIgnoreCase("/kick")) {

					if (this.getClient().getUserPermissionFor(2)) {

						Player User = null;
						User = getAnonymousState().getAuthenticationService()
								.getPlayer(msgSplit[1]);
						
						if (User != null) {
							
							this.lobby.kickClient(User, this.getClient(), false, false);
														
						}else{
							this.getClient().send(new MessageMessage("System", msgSplit[1]+" user Not found."));
						}
						sendMSG = false;
					}
					
				// Ban and Kick User Modulo Permission = 4
				}else if (msgSplit[0].equalsIgnoreCase("/ban")) {
					if (this.getClient().getUserPermissionFor(4)) {

						Player User = null;
						User = getAnonymousState().getAuthenticationService()
								.getPlayer(msgSplit[1]);
						
						if (User != null) {
							
							this.lobby.kickClient(User, this.getClient(), true, true);
														
						}else{
							this.getClient().send(new MessageMessage("System", msgSplit[1]+" user Not found."));
						}
						sendMSG = false;
					}
				// UnBan User Modulo Permission = 8
				}else if (msgSplit[0].equalsIgnoreCase("/unban")) {
					if (this.getClient().getUserPermissionFor(8)) {

						Player User = null;
						User = getAnonymousState().getAuthenticationService()
								.getPlayer(msgSplit[1]);
						
						if (User != null) {
							
							this.lobby.unBanClient(User, this.getClient());
														
						}else{
							this.getClient().send(new MessageMessage("System", msgSplit[1]+" user Not found."));
						}
						sendMSG = false;
					}
				}
			}
			
			if (sendMSG == true) this.lobby.sendMessage(this.getClient(), ((SendMessageMessage) message).getMessage());
			
			return this;
		} else if (message instanceof RefreshMessage) {
			this.lobby.refresh(this.getClient());
			return this;
		} else if (message instanceof ScoreRequestMessage) {
			ScoreRequestMessage requestMessage = (ScoreRequestMessage) message;
			ScoreResponseMessage responseMessage =  
				HighscoreService.getScoreMessage(requestMessage.getPlayerName());
			this.getClient().send(responseMessage);
			return this;
		} else if (message instanceof HighscoreRequestMessage) {
			HighscoreRequestMessage requestMessage = (HighscoreRequestMessage) message;
			this.getClient().send(new HighscoreService().
					getHighscoreMessage(requestMessage.getStart()));
			return this;
		} else if (message instanceof CreateGameMessage) {
			Game game = this.lobby.createGame(this.getClient(),
					((CreateGameMessage) message).getGameName(),
					((CreateGameMessage) message).getMapId(),
					((CreateGameMessage) message).getMaxPlayers(),
					((CreateGameMessage) message).getPasswort(),
					((CreateGameMessage) message).getMaxEloPoints(),
					((CreateGameMessage) message).getMinEloPoints());
			
			if (game == null) {
				authenticatedStateLogger.error("failed to create game "
						+ ((CreateGameMessage) message).getGameName());
				return this;
			} else {
				return new InGameState(this.getOutQueue(), this.getClient(),
						game.getQueue(), this, this.getAuthenticationService());
			}
		} else if (message instanceof JoinGameRequestMessage) {
			
						
			Game game = this.lobby.joinGame(this.getClient(),
					((JoinGameRequestMessage) message).getGameId(),((JoinGameRequestMessage) message).getPasswort());
			
			if (game != null) {
	
				InGameState inGame = new InGameState(this.getOutQueue(), this
						.getClient(), game.getQueue(), this, 
						this.getAuthenticationService());
				authenticatedStateLogger.info("client "
						+ this.getClient().getUserName() + " joined game "
						+ game.getGameName() + " with gameid "
						+ game.getGameId());
				
				return inGame;
			} else {
				authenticatedStateLogger.error("failed to join game "
						+ ((JoinGameRequestMessage) message).getGameId());
				return this;
			}
		} else if (message instanceof LogoutMessage) {
			this.lobby.leaveLobby(this.getClient());
			this.logout();
			return this.anonymousState;
		} else {
			String errorMessage = "cannot handle message: " + message;
			this.getClient().handleError(IConstants.ErrorType.Error,  errorMessage);
			authenticatedStateLogger.error(errorMessage);
			return this;
		}
	}

	/**
	 * Returns the Lobby.
	 * 
	 * @return Lobby
	 */
	public Lobby getLobby() {
		return this.lobby;
	}

	/**
	 * Returns the AnonymousState.
	 * @return the AnonymousState.
	 */
	public AnonymousState getAnonymousState() {
		return this.anonymousState;
	}
}
