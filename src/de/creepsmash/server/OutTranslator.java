
/**
   Creep Smash, a multiplayer towerdefence game
   created as a project at the Hochschule fuer
   Technik Stuttgart (University of Applied Science)
   http://www.hft-stuttgart.de 
   
   Copyright (C) 2008 by      
    * Andreas Wittig
    * Bernd Hietler
    * Christoph Fritz
    * Fabian Kessel
    * Levin Fritz
    * Nikolaj Langner
    * Philipp Schulte-Hubbert
    * Robert Rapczynski
    * Ron Trautsch
    * Sven Supper
    http://creepsmash.sf.net/

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/


package de.creepsmash.server;

import java.io.OutputStream;
import java.io.PrintWriter;

import org.apache.log4j.Logger;

import de.creepsmash.common.messages.server.ServerMessage;

/**
 * Out-Translator for messages from Server to Client. 
 * Each client has an OutTranslator.
 * 
 * @author andreas
 */
public class OutTranslator
	implements QueueConsumerThread.Consumer<ServerMessage> {

	private final PrintWriter printWriter;
	private final int clientID;

	private static Logger logger = Logger.getLogger(OutTranslator.class); 


	/**
	 * @param outputStream the output stream for the client
	 * @param clientID the client's id (used for logging)
	 */
	public OutTranslator(OutputStream outputStream, int clientID) {
		if (outputStream == null) {
			throw new IllegalArgumentException("'outputStream' was null");
		}
		this.printWriter = new PrintWriter(outputStream);
		this.clientID = clientID;
	}

	/**
	 * @param message message to consume.
	 */
	public void consume(ServerMessage message) {
		if (message != null) {
			String messageString = message.getMessageString();
			this.printWriter.println(messageString);
			this.printWriter.flush();
			logger.debug("sent to client " 
					+ this.clientID + ": " + messageString);
		}
	}

}
