
/**
   Creep Smash, a multiplayer towerdefence game
   created as a project at the Hochschule fuer
   Technik Stuttgart (University of Applied Science)
   http://www.hft-stuttgart.de 
   
   Copyright (C) 2008 by      
    * Andreas Wittig
    * Bernd Hietler
    * Christoph Fritz
    * Fabian Kessel
    * Levin Fritz
    * Nikolaj Langner
    * Philipp Schulte-Hubbert
    * Robert Rapczynski
    * Ron Trautsch
    * Sven Supper
    http://creepsmash.sf.net/

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/


package de.creepsmash.server.game;

import org.apache.log4j.Logger;

import de.creepsmash.common.messages.client.BuildCreepMessage;
import de.creepsmash.common.messages.client.BuildTowerMessage;
import de.creepsmash.common.messages.client.ExitGameMessage;
import de.creepsmash.common.messages.client.GameMessage;
import de.creepsmash.common.messages.client.GameOverMessage;
import de.creepsmash.common.messages.client.KickPlayerRequestMessage;
import de.creepsmash.common.messages.client.LogoutMessage;
import de.creepsmash.common.messages.client.SellTowerMessage;
import de.creepsmash.common.messages.client.SendMessageMessage;
import de.creepsmash.common.messages.client.StartGameRequestMessage;
import de.creepsmash.common.messages.client.UpgradeTowerMessage;

/**
 * GameState for a game that has ended, meaning that all clients have sent
 * GAME_OVER, but there are still clients lurking around. Chat still works in
 * this state, but that's about it.
 */
public class EndedGameState extends GameState {

	private static Logger logger = Logger.getLogger(EndedGameState.class);

	/**
	 * Creates a new one.
	 * @param game the game. Must not be null.
	 */
	public EndedGameState(Game game) {
		super(game);
	}

	/**
	 * Handle a message.
	 * @param message the message
	 * @param sender the player who sent the message.
	 * @return the new state.
	 */
	public GameState consume(GameMessage message, PlayerInGame sender) {
		if (message == null) {
			throw new IllegalArgumentException("'message' was null!");
		}
		if (sender == null) {
			throw new IllegalArgumentException("'sender' was null!");
		}

		if (message instanceof ExitGameMessage) {
			return handle((ExitGameMessage) message);
		} else if (message instanceof SendMessageMessage) {
			handle((SendMessageMessage) message);
		} else if (message instanceof LogoutMessage) {
			return handle((LogoutMessage) message);
		} else if (message instanceof BuildTowerMessage
				|| message instanceof UpgradeTowerMessage
				|| message instanceof SellTowerMessage
				|| message instanceof BuildCreepMessage
				|| message instanceof GameOverMessage
				|| message instanceof StartGameRequestMessage
				|| message instanceof KickPlayerRequestMessage) {
			wrongStateForMessage(message, sender);
		} else {
			logger.error("cannot handle message: " + message);
		}
		return this;
	}

	/**
	 * Returns a string identifying this state.
	 * @return "ended"
	 */
	public String toString() {
		return "ended";
	}

}
