
/**
   Creep Smash, a multiplayer towerdefence game
   created as a project at the Hochschule fuer
   Technik Stuttgart (University of Applied Science)
   http://www.hft-stuttgart.de 
   
   Copyright (C) 2008 by      
    * Andreas Wittig
    * Bernd Hietler
    * Christoph Fritz
    * Fabian Kessel
    * Levin Fritz
    * Nikolaj Langner
    * Philipp Schulte-Hubbert
    * Robert Rapczynski
    * Ron Trautsch
    * Sven Supper
    http://creepsmash.sf.net/

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/


package de.creepsmash.server.game;

import org.apache.log4j.Logger;

/**
 * A thread that periodically calls a TickReceiver instance's {@link
 * TickReceiver#tick tick} method.
 */
public class TickThread extends Thread {
	private boolean shouldTerminate;
	private TickReceiver receiver;
	private long interval;

	private static Logger logger = Logger.getLogger(TickThread.class);


	/**
	 * An object that receives 'ticks'.
	 */
	public interface TickReceiver {
		/**
		 * Called periodically by TickThread.
		 */
		void tick();
	}

	/**
	 * Create the thread.
	 * 
	 * @param receiver
	 *            the TickReceiver instance
	 * @param interval
	 *            the interval (in milliseconds) between two ticks.
	 */
	public TickThread(TickReceiver receiver, long interval) {
		this.shouldTerminate = false;
		this.receiver = receiver;
		this.interval = interval;
	}

	/**
	 * Start the thread.
	 */
	public void run() {
		long nextCheck = System.currentTimeMillis();
		while (true) {
			long currentTime = System.currentTimeMillis();
			long diff = nextCheck - currentTime;
			nextCheck += this.interval;
			if (shouldTerminate()) {
				break;
			}

			this.receiver.tick();
			if (shouldTerminate()) {
				break;
			}

			try {
				if (interval + diff > 0) {
					Thread.sleep(this.interval + diff);
				}
			} catch (InterruptedException e) {
				logger.debug("Servertick interrupted.");
			}
		}
	}

	/**
	 * Checks whether the thread has been asked to terminate. In a separate
	 * method because it needs to be synchronized.
	 * 
	 * @return true if the thread should now terminate
	 */
	private synchronized boolean shouldTerminate() {
		return this.shouldTerminate;
	}

	/**
	 * Ask the thread to terminate gracefully.
	 */
	public synchronized void terminate() {
		this.shouldTerminate = true;
		this.interrupt();
	}
}
