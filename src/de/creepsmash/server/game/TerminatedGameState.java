
/**
   Creep Smash, a multiplayer towerdefence game
   created as a project at the Hochschule fuer
   Technik Stuttgart (University of Applied Science)
   http://www.hft-stuttgart.de 
   
   Copyright (C) 2008 by      
    * Andreas Wittig
    * Bernd Hietler
    * Christoph Fritz
    * Fabian Kessel
    * Levin Fritz
    * Nikolaj Langner
    * Philipp Schulte-Hubbert
    * Robert Rapczynski
    * Ron Trautsch
    * Sven Supper
    http://creepsmash.sf.net/

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/


package de.creepsmash.server.game;

import org.apache.log4j.Logger;

import de.creepsmash.common.messages.client.GameMessage;

/**
 * GameState for a game that has terminated, meaning that there are no players
 * left.
 */
public class TerminatedGameState extends GameState {

	private static Logger logger = Logger.getLogger(TerminatedGameState.class);

	/**
	 * Creates a new one.
	 * @param game the game. Must not be null.
	 */
	public TerminatedGameState(Game game) {
		super(game);
	}

	/**
	 * Handle a message. All messages are ignored.
	 * @param message the message
	 * @param sender the player who sent the message.
	 * @return this.
	 */
	public GameState consume(GameMessage message, PlayerInGame sender) {
		logger.info("ignoring message: " + message);
		return this;
	}

	/**
	 * Returns a string identifying this state.
	 * @return "terminated"
	 */
	public String toString() {
		return "terminated";
	}

}
