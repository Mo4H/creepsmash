
/**
   Creep Smash, a multiplayer towerdefence game
   created as a project at the Hochschule fuer
   Technik Stuttgart (University of Applied Science)
   http://www.hft-stuttgart.de 
   
   Copyright (C) 2008 by      
    * Andreas Wittig
    * Bernd Hietler
    * Christoph Fritz
    * Fabian Kessel
    * Levin Fritz
    * Nikolaj Langner
    * Philipp Schulte-Hubbert
    * Robert Rapczynski
    * Ron Trautsch
    * Sven Supper
    http://creepsmash.sf.net/

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/


package de.creepsmash.server.game;

import de.creepsmash.server.Client;

/**
 * Represents a player participating in a game.
 */
public class PlayerInGame {
	private Client client;
	private boolean gameOver;

	/**
	 * Creates a new one with the given client and gameOver = false.
	 * @param client the client. Must not be null.
	 */
	public PlayerInGame(Client client) {
		if (client == null) {
			throw new IllegalArgumentException("'client' was null");
		}
		this.client = client;
		this.gameOver = false;
	}

	/**
	 * Returns the client.
	 * @return the client.
	 */
	public Client getClient() {
		return this.client;
	}

	/**
	 * The gameOver flag (true if this client has sent a GAME_OVER message).
	 * @return the gameOver flag.
	 */
	public boolean getGameOver() {
		return this.gameOver;
	}
	/**
	 * Sets the gameOver flag.
	 */
	public void gameOver() {
		this.gameOver = true;
	}
}
