
/**
   Creep Smash, a multiplayer towerdefence game
   created as a project at the Hochschule fuer
   Technik Stuttgart (University of Applied Science)
   http://www.hft-stuttgart.de 
   
   Copyright (C) 2008 by      
    * Andreas Wittig
    * Bernd Hietler
    * Christoph Fritz
    * Fabian Kessel
    * Levin Fritz
    * Nikolaj Langner
    * Philipp Schulte-Hubbert
    * Robert Rapczynski
    * Ron Trautsch
    * Sven Supper
    http://creepsmash.sf.net/

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/


package de.creepsmash.server;

import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;

import de.creepsmash.common.messages.client.ClientMessage;
import de.creepsmash.common.messages.server.ServerMessage;

/**
 * Implements the basics for the different States of a client:
 * InGameState, AnonymousState and AuthenticatedState. 
 * @author Bernd
 */
public abstract class ClientState {
	
	/**
	 * The queue for messages send from server to client.  
	 */
	private BlockingQueue<QueueMessage<ServerMessage>> outQueue;
	protected final AuthenticationService authenticationService;
	private Client client;	

	private static Logger clientStateLogger = Logger.getLogger(ClientState.class); 

	/**
	 * Initiates the outQeue.
	 * @param outQueue
	 * Expects an BlockingQueue object with the ServerMessage.
	 * @param client Client
	 * @param authenticationService the AuthenticationService
	 */
	public ClientState(BlockingQueue<QueueMessage<ServerMessage>> outQueue,
			Client client, AuthenticationService authenticationService) {
		if (outQueue == null) {
			throw new IllegalArgumentException("'outQueue' was null");
		}
		if (client == null) {
			throw new IllegalArgumentException("'client' was null");
		}
		if (authenticationService == null) {
			throw new IllegalArgumentException("'authenticationService' was null");
		}
		this.outQueue = outQueue;
		this.client = client;
		this.authenticationService = authenticationService;
	}

	/**
	 * Sends messages to the client. 
	 * @param message Expects a ServerMessage object.
	 * @return the new ClientState.
	 */
	public ClientState sendMessage(ServerMessage message) {
		try {
			outQueue.put(new QueueMessage<ServerMessage>(message));
			return this;
		} catch (InterruptedException e) {
			clientStateLogger.error("Sendmessage failed." , e);
			return this;
		}
	}
	
	/**
	 * receive messages from Client. 
	 * @param message
	 * Expects a ClientMessage.
	 * @return ClientState
	 */
	public abstract ClientState receiveMessage(ClientMessage message); 
	
	/**
	 * Returns the Client.
	 * @return Client
	 */
	protected Client getClient() {
		return this.client;
	}
	
	
	/***
	 * Returns the outqueue.
	 * @return BlockingQueue
	 */
	protected BlockingQueue<QueueMessage<ServerMessage>> getOutQueue() {
		return this.outQueue;
	}

	/**
	 * Returns the AuthenticationService.
	 * @return the AuthenticationService.
	 */
	protected AuthenticationService getAuthenticationService() {
		return this.authenticationService;
	}

	/**
	 * Log out.
	 */
	protected void logout() {
		this.authenticationService.logout(this.client.getUserName());
	}
	
}
