
/**
   Creep Smash, a multiplayer towerdefence game
   created as a project at the Hochschule fuer
   Technik Stuttgart (University of Applied Science)
   http://www.hft-stuttgart.de 
   
   Copyright (C) 2008 by      
    * Andreas Wittig
    * Bernd Hietler
    * Christoph Fritz
    * Fabian Kessel
    * Levin Fritz
    * Nikolaj Langner
    * Philipp Schulte-Hubbert
    * Robert Rapczynski
    * Ron Trautsch
    * Sven Supper
    http://creepsmash.sf.net/

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/


package de.creepsmash.server;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.util.logging.Logger;

/**
 * A pseudo client which sends a shutdown message to the Server for a clean
 * shutdown. Before shutting down an error-message is send to all connected
 * clients. This works only if the pseudo-client is launched on the server's
 * localhost.
 * 
 * @author Bernd Hietler
 * 
 */
public class ShutdownTD {

	private String host = "localhost";
	private int port = 4747;
	private Socket socket;
	private PrintWriter out;
	private Logger shutdownLogger = Logger.getLogger(this.getClass().getName());

	/**
	 * Main-method.
	 * 
	 * @param args
	 *            arguments
	 */
	public static void main(String[] args) {

		ShutdownTD stopper = new ShutdownTD();
		stopper.makeContact();
		stopper.sendMessage();
	}

	/**
	 * contact the server.
	 */
	public void makeContact() {
		try {
			this.socket = new Socket(host, port);

			this.out = new PrintWriter(new OutputStreamWriter(this.socket
					.getOutputStream(), Charset.forName("UTF-8")), true);

		} catch (UnknownHostException e) {
			shutdownLogger.info("Host not found. ");
			e.printStackTrace();
		} catch (IOException e) {
			shutdownLogger.info("Input/Output Error: ");
			e.printStackTrace();
		}
	}

	/**
	 * Method to send the shutdown message to the server.
	 */
	public void sendMessage() {
		shutdownLogger.info("Shutting down server " + host + ":" + port);
		this.out.println("SERVER_SHUTDOWN");
	}
}
