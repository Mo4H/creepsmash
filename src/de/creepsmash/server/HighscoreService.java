
/**
   Creep Smash, a multiplayer towerdefence game
   created as a project at the Hochschule fuer
   Technik Stuttgart (University of Applied Science)
   http://www.hft-stuttgart.de 
   
   Copyright (C) 2008 by      
    * Andreas Wittig
    * Bernd Hietler
    * Christoph Fritz
    * Fabian Kessel
    * Levin Fritz
    * Nikolaj Langner
    * Philipp Schulte-Hubbert
    * Robert Rapczynski
    * Ron Trautsch
    * Sven Supper
    http://creepsmash.sf.net/

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/


package de.creepsmash.server;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.apache.log4j.Logger;

import de.creepsmash.common.messages.server.HighscoreEntry;
import de.creepsmash.common.messages.server.HighscoreResponseMessage;
import de.creepsmash.common.messages.server.ScoreResponseMessage;
import de.creepsmash.server.model.Player;

/**
 * Service for the highscores.
 * 
 * @author andreas, nikolaj
 * 
 */
public class HighscoreService {

	private static Logger logger = Logger.getLogger(HighscoreService.class);

	public double[] eloPoints;
	public double[] newEloPoints;
	public double[] eVal;

	public static class ExtendedPlayer {

		private Player player;

		private int postion;
		
		/**
		 * constructor of extended player.
		 * 
		 */
		public ExtendedPlayer(Player player, int postion) {
			super();
			this.player = player;
			this.postion = postion;
		}

		/**
		 * @return the player
		 */
		public Player getPlayer() {
			return this.player;
		}

		/**
		 * @return the postion
		 */
		public int getPostion() {
			return this.postion;
		}

	}

	/**
	 * @param playerName the username.
	 * @return a ScoreResponseMessage with the user's data.
	 */
	public static ScoreResponseMessage getScoreMessage(String playerName) {
		ScoreResponseMessage scoreResponseMessage = new ScoreResponseMessage();
		Player player = AuthenticationService.getPlayer(playerName);
		if (player != null) {
			scoreResponseMessage.setPlayerName(player.getName());
			scoreResponseMessage.setPoints(player.getElopoints() - 500);
			scoreResponseMessage.setOldPoints(player.getOldElopoints() - 500);	
		}
		return scoreResponseMessage;
	}
		
	/**
	 * @param offset 
	 * @return the actual HighscoreResponseMessage to send to clients.
	 */
	public HighscoreResponseMessage getHighscoreMessage(int offset) {
		HighscoreResponseMessage highscoreResponseMessage = new HighscoreResponseMessage();
		Set<HighscoreEntry> highscoreEntries = new HashSet<HighscoreEntry>();

		Set<Player> players = AuthenticationService.getPlayers(offset);
		for (Player player : players) {
			HighscoreEntry highscoreEntry = new HighscoreEntry();
			highscoreEntry.setPlayerName(player.getName());
			highscoreEntry.setPoints(player.getElopoints() - 500);
			highscoreEntry.setOldPoints(player.getOldElopoints() - 500);
			highscoreEntries.add(highscoreEntry);
		}
		highscoreResponseMessage.setHighscoreEntries(highscoreEntries);
		return highscoreResponseMessage;
	}

	/**
	 * Updates the highscore table with the results of a game.
	 * 
	 * @param playerNamePositionMap
	 *            maps player names to position (position 1 means the player won
	 *            the game etc)
	 */
	public void createHighscoreEntry(Map<String, Integer> playerNamePositionMap) {
		try {
			EntityManager entityManager = PersistenceManager.getInstance()
					.getEntityManager();

			HashSet<ExtendedPlayer> players = new HashSet<ExtendedPlayer>();
			for (String playerName : playerNamePositionMap.keySet()) {
				Player player = AuthenticationService.getPlayer(playerName);
				if (player != null) {
					players.add(new ExtendedPlayer(player,
							playerNamePositionMap.get(playerName)));
				}
			}

			ArrayList<ExtendedPlayer> pl = new ArrayList<ExtendedPlayer>(
					players);

			Collections.sort(pl, new Comparator<ExtendedPlayer>() {
				public int compare(ExtendedPlayer p1, ExtendedPlayer p2) {
					return p1.getPostion() - p2.getPostion();
				}
			});

			this.eloPoints = new double[pl.size()];

			for (int i = 0; i < pl.size(); i++) {
				this.eloPoints[i] = pl.get(i).getPlayer().getElopoints();
			}

			this.calceVal(pl, this.eloPoints);

			EntityTransaction entityTransaction = entityManager
					.getTransaction();
			entityTransaction.begin();

			this.calcEloPoints(pl, this.eloPoints);

			for (int i = 0; i < this.newEloPoints.length; i++) {
				
				pl.get(i).player.setOldElopoints((int) this.newEloPoints[i]
						- (pl.get(i).player.getElopoints() - 500));
				pl.get(i).player.setElopoints((int) this.newEloPoints[i]);
				entityManager.merge(pl.get(i).getPlayer());
			}

			entityTransaction.commit();
			logger.debug("highscores saved");
		} catch (Throwable t) {
			logger.error("error while saving highscores", t);
		}
	}

	/**
	 * @param pl
	 * @param ePoints
	 * 
	 */
	public void calcEloPoints(ArrayList<ExtendedPlayer> pl, double[] ePoints) {

		this.newEloPoints = new double[pl.size()];

		for (int j = 0; j < pl.size(); j++) {

			if (pl.size() == 4) {
				
				// Bsp:
				// alle haben 0 punkte:
				// 1. +52, 2. +31, 3. +10, 4. -17.5 
				// S1 hat 400P Vorsprung und gewinnt
				// S1 bekommt +16, 2. +43, 3. +22, 4. -0
				// S1 hat 400P Vorsprung und wird 2.
				// S1 bekommt -4, 1. +64, 3. + 22, 4. -0
				
				//70
				int k = 70;
				
				double[] pos = { 1.0, 0.7, 0.4, 0.0 };

				this.newEloPoints[j] = (Math.ceil(ePoints[j] + k
						* (pos[j] - this.eVal[j])));

				if (this.newEloPoints[j] < 500.0) {
					this.newEloPoints[j] = 500.0;
				}
			}
			
			// Bsp.
			// Alle Spieler haben 0 Punkte
			// 1. +40, 2. +10, 3. -20

			if (pl.size() == 3) {
				
				//60
				int k = 60;

				double[] pos = { 1.0, 0.5, 0.0 };


				this.newEloPoints[j] = (Math.ceil(ePoints[j] + k
						* (pos[j] - this.eVal[j])));

				if (this.newEloPoints[j] < 500) {
					this.newEloPoints[j] = 500;
				}
			}

			
			// Bsp.
			// Beide Spieler haben 0 Punkte
			// 1. +25, 2. -25
			// S1 hat 500P Vorsprung
			// S1 verliert bekommt -47P, S2 bekommt +47
			// S1 gewinnt bekommt +2, S2 bekommt -2
			// ab 676P Vorsprung bekommt S1 auch bei einem Sieg 
			// keine Punkte mehr
			
			
			if (pl.size() == 2) {
				
				int k = 50;
				
				double[] pos = { 1.0, 0.0 };

				this.newEloPoints[j] = (Math.ceil(ePoints[j] + k
						* (pos[j] - this.eVal[j])));

				if (this.newEloPoints[j] < 500) {
					this.newEloPoints[j] = 500;
				}
			}
		}
	}

	/**
	 * @param pl
	 * @param ePoints
	 */
	public void calceVal(ArrayList<ExtendedPlayer> pl, double[] ePoints) {

		this.eVal = new double[pl.size()];

		for (int i = 0; i < ePoints.length; i++) {

			if (ePoints.length == 4) {
				this.eVal[i] = (Math.pow(10, (ePoints[i] / 400)))
						/ ((Math.pow(10, (ePoints[0] / 400)))
								+ (Math.pow(10, (ePoints[1] / 400)))
								+ (Math.pow(10, (ePoints[2] / 400))) + (Math
								.pow(10, (ePoints[3] / 400))));
			}

			if (ePoints.length == 3) {
				this.eVal[i] = (Math.pow(10, (ePoints[i] / 400)))
						/ ((Math.pow(10, (ePoints[0] / 400)))
								+ (Math.pow(10, (ePoints[1] / 400))) + (Math
								.pow(10, (ePoints[2] / 400))));
			}

			if (ePoints.length == 2) {
				this.eVal[i] = (Math.pow(10, (ePoints[i] / 400)))
						/ ((Math.pow(10, (ePoints[0] / 400))) + (Math.pow(10,
								(ePoints[1] / 400))));
			}
		}
	}
}
