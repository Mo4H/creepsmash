
/**
   Creep Smash, a multiplayer towerdefence game
   created as a project at the Hochschule fuer
   Technik Stuttgart (University of Applied Science)
   http://www.hft-stuttgart.de 
   
   Copyright (C) 2008 by      
    * Andreas Wittig
    * Bernd Hietler
    * Christoph Fritz
    * Fabian Kessel
    * Levin Fritz
    * Nikolaj Langner
    * Philipp Schulte-Hubbert
    * Robert Rapczynski
    * Ron Trautsch
    * Sven Supper
    http://creepsmash.sf.net/

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/


package de.creepsmash.server;

import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;

/**
 * Takes objects from a queue and feeds them to a {@link
 * QueueConsumerThread.Consumer Consumer} instance.
 * @param <T> the type of the objects in the queue.
 */
public class QueueConsumerThread<T> extends Thread {
	private Consumer<T> consumer;
	private BlockingQueue<QueueMessage<T>> queue;
	
	private static Logger logger = Logger.getLogger(QueueConsumerThread.class);

	/**
	 * Objects that receive objects from a QueueConsumerThread must implement
	 * this interface.
	 */
	public interface Consumer<T> {
		/**
		 * Consume a message.
		 * @param o the message
		 */
		void consume(T o);
	}

	/**
	 * Create a new one. After creating one, you still have to call start()
	 * before starts to run!
	 * @param consumer the object that will be passed objects from
	 * @param queue the queue.
	 */
	public QueueConsumerThread(Consumer<T> consumer,
			BlockingQueue<QueueMessage<T>> queue) {
		if (consumer == null) {
			throw new IllegalArgumentException("'consumer' was null");
		}
		if (queue == null) {
			throw new IllegalArgumentException("'queue' was null");
		}
		this.consumer = consumer;
		this.queue = queue;
	}

	/**
	 * The Thread's run-method.
	 */
	public void run() {
		while (true) {
			QueueMessage<T> m = null;
			try {
				m = this.queue.take();
			} catch (InterruptedException e) {
				logger.debug("caught InterruptedException:", e);
			}
			if (m == null) {
				continue;
			}
			if (m.isSentinel()) {
				break;
			}
			try {
				this.consumer.consume(m.getContent());
			} catch (Exception e) {
				logger.error("caught Exception", e);
			}
		}
	}
}
