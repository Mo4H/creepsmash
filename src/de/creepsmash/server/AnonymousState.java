/**
 Creep Smash, a multiplayer towerdefence game
 created as a project at the Hochschule fuer
 Technik Stuttgart (University of Applied Science)
 http://www.hft-stuttgart.de 

 Copyright (C) 2008 by      
 * Andreas Wittig
 * Bernd Hietler
 * Christoph Fritz
 * Fabian Kessel
 * Levin Fritz
 * Nikolaj Langner
 * Philipp Schulte-Hubbert
 * Robert Rapczynski
 * Ron Trautsch
 * Sven Supper
 http://creepsmash.sf.net/

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

package de.creepsmash.server;

import java.util.concurrent.BlockingQueue;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceException;

import org.apache.log4j.Logger;

import de.creepsmash.common.IConstants;
import de.creepsmash.common.messages.client.ClientMessage;
import de.creepsmash.common.messages.client.LoginRequestMessage;
import de.creepsmash.common.messages.client.PasswordResetRequestMessage;
import de.creepsmash.common.messages.client.RegistrationRequestMessage;
import de.creepsmash.common.messages.client.ShutdownMessage;
import de.creepsmash.common.messages.client.UpdateDataRequestMessage;
import de.creepsmash.common.messages.server.LoginResponseMessage;
import de.creepsmash.common.messages.server.PasswordResetResponseMessage;
import de.creepsmash.common.messages.server.RegistrationResponseMessage;
import de.creepsmash.common.messages.server.ServerMessage;
import de.creepsmash.common.messages.server.UpdateDataResponseMessage;
import de.creepsmash.server.model.BlackList;
import de.creepsmash.server.model.Player;

/**
 * This class represents the state of the client when it is not yet logged in.
 * 
 * @author Bernd Hietler
 */
public class AnonymousState extends ClientState {

	private static Logger anonymousStateLogger = Logger
			.getLogger(AnonymousState.class);

	private final AuthenticationService authenticationService;

	/**
	 * Constructor.
	 * 
	 * @param outQueue
	 *            Expects an BlockingQueue object with the ServerMessage.
	 * @param client
	 *            Client
	 * @param authenticationService
	 *            the AuthenticationService
	 */
	public AnonymousState(BlockingQueue<QueueMessage<ServerMessage>> outQueue,
			Client client, AuthenticationService authenticationService) {
		super(outQueue, client, authenticationService);
		this.authenticationService = authenticationService;
	}

	/**
	 * Receiving messages from the client.
	 * 
	 * @param message
	 *            ClientMessage
	 * @return ClientState
	 */
	@Override
	public ClientState receiveMessage(ClientMessage message) {
		ClientState clientState = this;
		if (message == null) {
			anonymousStateLogger.info("client " + this.getClient()
					+ " disconnected in AnonymousState");
			return null;
		} else {
			if (message instanceof UpdateDataRequestMessage) {
				// must be logged in to update data
				UpdateDataResponseMessage m = new UpdateDataResponseMessage();
				m.setResponseType(IConstants.ResponseType.failed);
				this.getClient().send(m);
			} else if (message instanceof RegistrationRequestMessage) {
				RegistrationRequestMessage registrationRequestMessage = (RegistrationRequestMessage) message;
				IConstants.ResponseType responseType = AuthenticationService
						.create(registrationRequestMessage.getUsername(),
								registrationRequestMessage.getPassword(),
								registrationRequestMessage.getEmail());
				RegistrationResponseMessage registrationResponseMessage = new RegistrationResponseMessage();
				registrationResponseMessage.setResponseType(responseType);
				this.sendMessage(registrationResponseMessage);
			} else if (message instanceof PasswordResetRequestMessage) {
				PasswordResetRequestMessage passwordResetRequestMessage = (PasswordResetRequestMessage) message;
				boolean result = this.authenticationService
						.resetPassword(passwordResetRequestMessage
								.getPlayername());
				PasswordResetResponseMessage resetResponse = new PasswordResetResponseMessage();
				if (result) {
					resetResponse.setResponseType(IConstants.ResponseType.ok);
				} else {
					resetResponse
							.setResponseType(IConstants.ResponseType.failed);
				}
				this.getClient().send(resetResponse);
				return this;
			} else if (message instanceof LoginRequestMessage) {
				LoginRequestMessage loginRequestMessage = (LoginRequestMessage) message;
				String serverVersion = Server.getVersion();
				LoginResponseMessage loginResponseMessage = new LoginResponseMessage();
				String clientVersion = loginRequestMessage.getVersion();
				if (!serverVersion.equals(clientVersion)) {
					anonymousStateLogger.info("client " + this.getClient()
							+ " has wrong version: " + clientVersion);
					loginResponseMessage
							.setResponseType(IConstants.ResponseType.version);
					this.sendMessage(loginResponseMessage);
					return this;
				}
				EntityManager entityManager = PersistenceManager.getInstance().getEntityManager();
				BlackList bl = null;
				try {
					bl  = entityManager.find(BlackList.class, this.getClient().getIPAddress() );
					if (bl == null) {
						bl  = entityManager.find(BlackList.class, loginRequestMessage.getMacaddress() );
					}
				}catch(PersistenceException e){
					anonymousStateLogger.info("Connection lost ?");
				}
				if (bl != null) {
					anonymousStateLogger.info("blocked user try to login: " + loginRequestMessage.getUsername() );
					loginResponseMessage
							.setResponseType(IConstants.ResponseType.failed);
					this.sendMessage(loginResponseMessage);
					return this;
				}
				Lobby lobby = this.getAuthenticationService().login(
						loginRequestMessage.getUsername(),
						loginRequestMessage.getPassword());
				if (lobby == null) {
					loginResponseMessage
							.setResponseType(IConstants.ResponseType.failed);
					this.sendMessage(loginResponseMessage);
					anonymousStateLogger.error("login failed");
				} else {
					loginResponseMessage
							.setResponseType(IConstants.ResponseType.ok);
					this.getClient().setUserName(
							loginRequestMessage.getUsername());
					this.getClient().setMACAddress(
							loginRequestMessage.getMacaddress());
					
					Player player = entityManager.find(Player.class, loginRequestMessage.getUsername());
					EntityTransaction entityTransaction = entityManager.getTransaction();
					entityTransaction.begin();
					player.setLastlogin(System.currentTimeMillis()/1000L);
					player.setIp(this.getClient().getIPAddress());
					player.setMac(this.getClient().getMACAddress());
					entityManager.merge(player);
					entityManager.flush();
					entityTransaction.commit();
					
					this.sendMessage(loginResponseMessage);
					clientState = new AuthenticatedState(this.getOutQueue(),
							this.getClient(), lobby, this, this
									.getAuthenticationService());
					anonymousStateLogger.info(this.getClient().getUserName()
							+ " logged in");
				}
			} else if (message instanceof ShutdownMessage) {
				this.getClient().stopServer();
				return this;
			}
			return clientState;
		}
	}

}
