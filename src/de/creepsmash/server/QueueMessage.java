
/**
   Creep Smash, a multiplayer towerdefence game
   created as a project at the Hochschule fuer
   Technik Stuttgart (University of Applied Science)
   http://www.hft-stuttgart.de 
   
   Copyright (C) 2008 by      
    * Andreas Wittig
    * Bernd Hietler
    * Christoph Fritz
    * Fabian Kessel
    * Levin Fritz
    * Nikolaj Langner
    * Philipp Schulte-Hubbert
    * Robert Rapczynski
    * Ron Trautsch
    * Sven Supper
    http://creepsmash.sf.net/

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/


package de.creepsmash.server;

/**
 * QueueMessage wraps a message that's put in a queue and consumed by some
 * consumer. This way we can send special "sentinel" messages which signal the
 * consumer that it should terminate.
 * @param <T> the type of message that is to be wrapped
 */
public class QueueMessage<T> {
	private T t;
	private boolean isSentinel;

	/**
	 * No-arg constructor. Private because clients are supposed to call the
	 * factory method 'sentinel'.
	 */
	private QueueMessage() {
		this.isSentinel = true;
	}

	/**
	 * Create a regular (non-sentinel) message.
	 * @param t the object to wrap
	 */
	public QueueMessage(T t) {
		this.t = t;
		this.isSentinel = false;
	}

	/**
	 * Creates a sentinel. Call it like this:
	 * <code>QueueMessage.&lt;MyMessageType&gt;sentinel()</code>
	 * @param <U> the type of message
	 * @return a sentinel for the given type of message
	 */
	public static <U> QueueMessage<U> sentinel() {
		return new QueueMessage<U>();
	}

	/**
	 * Returns the actual message (for a regular queue message) or null (for a
	 * sentinel.
	 * @return the message or null.
	 */
	public T getContent() {
		return this.t;
	}

	/**
	 * Returns true if this message is a sentinel.
	 * @return true if this is a sentinel
	 */
	public boolean isSentinel() {
		return this.isSentinel;
	}
}
