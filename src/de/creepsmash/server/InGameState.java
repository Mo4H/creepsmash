/**
   Creep Smash, a multiplayer towerdefence game
   created as a project at the Hochschule fuer
   Technik Stuttgart (University of Applied Science)
   http://www.hft-stuttgart.de 
   
   Copyright (C) 2008 by      
    * Andreas Wittig
    * Bernd Hietler
    * Christoph Fritz
    * Fabian Kessel
    * Levin Fritz
    * Nikolaj Langner
    * Philipp Schulte-Hubbert
    * Robert Rapczynski
    * Ron Trautsch
    * Sven Supper
    http://creepsmash.sf.net/

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

package de.creepsmash.server;

import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;

import de.creepsmash.common.messages.client.ClientMessage;
import de.creepsmash.common.messages.client.DeleteRequestMessage;
import de.creepsmash.common.messages.client.ExitGameMessage;
import de.creepsmash.common.messages.client.GameMessage;
import de.creepsmash.common.messages.client.LogoutMessage;
import de.creepsmash.common.messages.client.ScoreRequestMessage;
import de.creepsmash.common.messages.client.UpdateDataRequestMessage;
import de.creepsmash.common.messages.server.DeleteResponseMessage;
import de.creepsmash.common.messages.server.KickedMessage;
import de.creepsmash.common.messages.server.ScoreResponseMessage;
import de.creepsmash.common.messages.server.ServerMessage;
import de.creepsmash.common.messages.server.UpdateDataResponseMessage;

/**
 * 
 * This class represents the state of the client when it is in an active game.
 * 
 * @author Bernd Hietler
 */

public class InGameState extends ClientState {

	private final BlockingQueue<QueueMessage<GameMessage>> gameQueue;
	private static Logger inGameLogger = Logger.getLogger(InGameState.class);
	private final AuthenticatedState authenticatedState;

	/**
	 * Constructor method instantiates the InGameState object.
	 * 
	 * @param outQueue BlockingQueue for ServerMessage objects.
	 * @param gameQueue BlockingQueue for GameMessage objects.
	 * @param client Client
	 * @param authenticatedState the previous state.
	 * @param authenticationService the AuthenticationService.
	 */
	public InGameState(BlockingQueue<QueueMessage<ServerMessage>> outQueue,
			Client client, BlockingQueue<QueueMessage<GameMessage>> gameQueue,
			AuthenticatedState authenticatedState,
			AuthenticationService authenticationService) {
		super(outQueue, client, authenticationService);
		this.gameQueue = gameQueue;
		this.authenticatedState = authenticatedState;
	}

	/**
	 * Handles the game-messages.
	 * 
	 * @param message
	 *            ClientMessage
	 * @return ClientState
	 */
	@Override
	public ClientState receiveMessage(ClientMessage message) {
		if (message == null) {
			LogoutMessage m = new LogoutMessage();
			m.setClientId(this.getClient().getClientID());
			try {
				this.gameQueue.put(new QueueMessage<GameMessage>(m));
			} catch (InterruptedException e) {
				inGameLogger.error("InterruptedException", e);
			}
			this.logout();
			inGameLogger.info(
					"client " + this.getClient() 
					+ " disconnected in InGameState");
			return null;
		} else if (message instanceof UpdateDataRequestMessage) {
			UpdateDataRequestMessage updateDataRequestMessage = 
				(UpdateDataRequestMessage) message;
						
			UpdateDataResponseMessage m = new UpdateDataResponseMessage();
			m.setResponseType(AuthenticationService.update(
					this.getClient().getUserName(), 
					updateDataRequestMessage.getOldPassword(), 
					updateDataRequestMessage.getPassword(), 
					updateDataRequestMessage.getEmail()));
			this.getClient().send(m);
			return this;
		} else if (message instanceof ScoreRequestMessage) {
			ScoreRequestMessage requestMessage = (ScoreRequestMessage) message;
			ScoreResponseMessage responseMessage =
				HighscoreService.getScoreMessage(requestMessage.getPlayerName());
			this.getClient().send(responseMessage);
			return this;
		} else if (message instanceof DeleteRequestMessage) {
			DeleteResponseMessage m = new DeleteResponseMessage();
			m.setResponseType(AuthenticationService.delete(
					this.getClient().getUserName()));
			this.getClient().send(m);
			return this;
		} else if (message instanceof GameMessage) {
			try {
				this.gameQueue.put(new QueueMessage<GameMessage>(
						(GameMessage) message));
			} catch (InterruptedException e) {
				inGameLogger.error(
						"Error in InGameState: InterruptedException", e);
			}
			if (message instanceof ExitGameMessage) {
				return this.backToLobby();
			} else if (message instanceof LogoutMessage) {
				this.logout();
				return this.authenticatedState.getAnonymousState();
			} else {
				return this;
			}
		} else {
			inGameLogger.error("Wrong messagetype for GameQueue : "
					+ message.getMessageString());
			return this;
		}
	}

	/**
	 * Go back to the lobby.
	 * @return authenticatedState.
	 */
	private ClientState backToLobby() {
		this.authenticatedState.getLobby().newClient(this.getClient());
		return this.authenticatedState;
	}

	/**
	 * Sends messages to the client.
	 * 
	 * @param message
	 *            Expects a ServerMessage object.
	 * @return ClientState
	 */
	@Override
	public ClientState sendMessage(ServerMessage message) {
		super.sendMessage(message);
		if (message instanceof KickedMessage) {
			return this.backToLobby();
		}
		return this;
	}
}
