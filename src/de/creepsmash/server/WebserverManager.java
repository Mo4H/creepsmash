
/**
   Creep Smash, a multiplayer towerdefence game
   created as a project at the Hochschule fuer
   Technik Stuttgart (University of Applied Science)
   http://www.hft-stuttgart.de 
   
   Copyright (C) 2008 by      
    * Andreas Wittig
    * Bernd Hietler
    * Christoph Fritz
    * Fabian Kessel
    * Levin Fritz
    * Nikolaj Langner
    * Philipp Schulte-Hubbert
    * Robert Rapczynski
    * Ron Trautsch
    * Sven Supper
    http://creepsmash.sf.net/

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/


package de.creepsmash.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.BindException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.mortbay.jetty.Handler;
import org.mortbay.jetty.Request;
import org.mortbay.jetty.handler.AbstractHandler;

/**
 * Manager for the embedded WebServer.
 * 
 * @author andreas
 *
 */
public class WebserverManager {

	private static Logger logger = Logger
	.getLogger(WebserverManager.class);
	
	/**
	 * Starts the embedded webserver.
	 * @param webserverPort the port for the webserver
	 * @param hostname the server's hostname.
	 * @param gameserverPort the port used for the game server.
	 */
	public static void start(final int webserverPort, final String hostname,
			final int gameserverPort) {
		org.mortbay.jetty.Server server = new org.mortbay.jetty.Server(webserverPort);

		final long time = (new Date()).getTime();
		
		Handler handler = new AbstractHandler()	{
			
		    public void handle(String target, HttpServletRequest request, 
		    		HttpServletResponse response, int dispatch)
		        throws IOException, ServletException   {
		    	
		    	//use index.html as default page
		    	if (target.equals("/")) {
		    		target = "/index.html";
		    	}
		    	
		    	//mime-type
		    	if (target.endsWith(".gif")) {
		    		response.setContentType("image/gif");
		    	} else if (target.endsWith(".jpeg") 
		    			|| target.endsWith(".jpg")) {
		    		response.setContentType("image/jpeg");
		    	} else if (target.endsWith(".htm") 
		    			|| target.endsWith(".html")) {
		    		response.setContentType("text/html");
		    	} else if (target.endsWith(".css")) {
		    		response.setContentType("text/css");
		    	} else if (target.endsWith(".jnlp")) {
		    		response.setContentType("application/x-java-jnlp-file");
		    	} else if (target.endsWith(".jar")) {
		    		response.setContentType("application/java-archive");
		    	} 
		 		    	
		        response.setStatus(HttpServletResponse.SC_OK);
		        response.setDateHeader("Date", time);
			response.setDateHeader("Last-Modified",  time);
		         		        
		        InputStream inputStream = 
		        	this.getClass().getResourceAsStream("/htdocs" + target);
		          		       	        
		        if (inputStream == null) {
		        	response.setStatus(HttpServletResponse.SC_NOT_FOUND);
		        	logger.warn("file not found: /htdocs" + target);
		        } else {  
		        	if (target.endsWith(".jnlp")) {
		        		 BufferedReader bufferedReader = 
		        			 new BufferedReader(
		        					 new InputStreamReader(inputStream));
		        		 while (bufferedReader.ready()) {
		        			 String line = bufferedReader.readLine();
		        			 line = line.replace("HOSTNAME", hostname);
		        			 line = line.replace(
		        					 "WEBSERVERPORT", 
		        					 String.valueOf(webserverPort));
		        			 line = line.replace(
		        					 "GAMESERVERPORT", 
		        					 String.valueOf(gameserverPort));
		        			 response.getOutputStream().println(line);
		        		 }
		        	} else {
				        while (inputStream.available() > 0) {
				        	response.getOutputStream().
				        	write(inputStream.read());
				        }
		        	}
		        }
		        
		        ((Request) request).setHandled(true);
		    }
		};
			
		server.setHandler(handler);	
		
		//start webserver
		try {
	    server.start();
		} catch (BindException e) {
			logger.error("could not start webserver, "
					+ "because port could not be used.");
		} catch (Exception e) {
			logger.error("error while starting webserver");
		}
	    
		
	}
	
}
